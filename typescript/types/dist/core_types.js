"use strict";
var Designation;
(function (Designation) {
    Designation[Designation["MANAGER"] = 9] = "MANAGER";
    Designation[Designation["EMPLOYEE"] = 100] = "EMPLOYEE";
    Designation[Designation["TEAMLEADER"] = 8] = "TEAMLEADER";
    Designation["INTERN"] = "INTERN";
})(Designation || (Designation = {}));
const myBio = {
    name: 'Ashutosh Panda',
    skill: ['javascript', 'typescript', 'python'],
    age: 21,
    userId: 1,
    qualification: {
        matriculation: 83.83,
        plustwo: 54.67,
    },
    role: [0o1, 'Operation MEAN'],
    position: Designation.INTERN,
    fatherName: 'Rajendra Kumar Panda',
};
const user = {
    name: 'Ashutosh Panda',
    skill: ['javascript', 'typescript', 'python'],
    age: 21,
    userId: 1,
    qualification: {
        matriculation: 83.83,
        plustwo: 54.67,
    },
    role: [0o1, 'Operation MEAN'],
    position: Designation.INTERN,
};
console.log(user.position);
