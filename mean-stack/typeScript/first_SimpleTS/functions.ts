function add(inputOne: number, inputTwo: number): number {
  return inputOne + inputTwo;
}

// Void Type ==> If there is no Return (a function not returning anything)

function printResult(params: number): void {
  console.log('Result: ' + params);
}
// console.log(typeof printResult);
// printResult(add(5, 79));

let combineValues: (a: number, b: number) => number;

combineValues = add;
// combineValues = printResult
// combineValues = 8; Wrong Becase combineValues is Type of Function

// console.log(combineValues(7, 8));

function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

// addAndHandle(20, 20, (result) => console.log(result));

function sendRequest(data: string, cb: (response: any) => void) {
  return cb({ data: 'Hi there!' });
}

sendRequest('Send this!', (response) => {
  console.log(response);
  return true;
});
