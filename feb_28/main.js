// const mA = [
//   [1, 2, 3],
//   [1, 2, 3],
//   [1, 2, 3]
// ];

// const mB = [
//   [9, 8, 11],
//   [9, 8, 11],
//   [9, 8, 11]
// ];
// function matrixMulti(mA, mB) {
//   let result = new Array(mA.length);
//   for (let i = 0; i < result.length; i++) {
//     result[i] = new Array(mB[i].length);
//     for (let j = 0; j < mB[i].length; j++) {
//       result[i][j] = 0;
//       for (let k = 0; k < mA[i].length; k++) {
//         result[i][j] += mA[i][k] * mB[k][j];
//       }
//     }
//   }
//   return result;
// }

// console.log(matrixMulti(mA, mB));

// const str = 'Bitcoin take over the !!! world Maybe who knows perhaps Asutosh ';
// function findShort(input) {
//   const newStr = input.match(/[A-Za-z]+/g).sort();
//   const sortedArr = newStr.sort((a, b) => a.length - b.length);
//   return sortedArr
// }

// console.log(findShort(str));

// function longestWord(str) {
//   const wordArr = str.toLowerCase().match(/[a-z0-9]+/g);
//   console.log(wordArr);
//   const sortedSen = wordArr.sort((a, b) => b.length - a.length);
// // console.log(sortedSen);
//   const longestWordArr = sortedSen.filter(
//     word => word.length === sortedSen[0].length
//   );
// //   console.log(longestWordArr);
//   longestWordArr.sort();
//   console.log(longestWordArr);
//   if (longestWordArr.length > 1) {
//     return longestWordArr[0];
//   } else {
//     return longestWordArr;
//   }

//   //   console.log(longestWordArr);
// }

// console.log(longestWord(str));

// const num = [
//   1,
//   2,
//   3,
//   4,
//   5,
//   6,
//   8,
//   5,
//   75,
//   454,
//   455,
//   456,
//   457,
//   458,
//   459,
//   460,
//   4,
//   64,
//   6
// ];
// const blankNum = [];
// let prev = 0;

// num.forEach(a => {
//   if (a === 1) {
//     blankNum.push([]);
//   } else if (a - prev !== 1) {
//     blankNum.push([]);
//   }

//   blankNum[blankNum.length - 1].push(a);
//   prev = a;
// });

// const finalAns = blankNum.sort((a, b) => b.length - a.length);
// console.log(finalAns[0]);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// const str = 'Bitcoin take over the !!! world Maybe who knows perhaps Ashutosh';

// function fiveOrMore(str) {
//   const wordArr = str.toLowerCase().match(/[a-z0-9]+/g);

//   const sorted = wordArr.map(word => {
//     if (word.length >= 5) {
//       //   console.log(...word);
//       return [...word].reverse().join('');
//     } else {
//       return word;
//     }
//   });
//   console.log(sorted.join(' '));
// }

// fiveOrMore(str);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// filter_list([1, 2, 'a', 'b']) == [1, 2];
// filter_list([1, 'a', 'b', 0, 15]) == [1, 0, 15];
// filter_list([1, 2, 'aasf', '1', '123', 123]) == [1, 2, 123];

// const rawData = [1, 2, 'aasf', '1', '123', 123];
// const ans = rawData.join(' ')
// console.log(ans.match(/[0-9]+/g));

// for(let i = 0; i < rawData.length; i++) {
//     if(!isNaN(rawData[i])) {
//         console.log(rawData[i]);
//     }
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Find Missing Number
// const arr = [2, 3, 4, 6];

// function findMiss() {
//   for (let i = 0; i < arr.length; i++) {
//     if (arr[i] + 1 !== arr[i + 1]) {
//       return arr[i] + 1;
//     }
//   }
// }

// console.log(findMiss(arr));
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Return the number (count) of vowels in the given string.
// We will consider a, e, i, o, and u as vowels for this Kata.
// The input string will only consist of lower case letters and/or spaces.

// const string = 'abcvhasdhaeiouuu hdhhd';
// const tmdString = string.match(/[a-zA-Z]/g);
// let counts = 0;
// function vowelReturn(string) {
//   for (let i = 0; i < tmdString.length; i++) {
//     if (
//       tmdString[i] === 'a' ||
//       tmdString[i] === 'e' ||
//       tmdString[i] === 'i' ||
//       tmdString[i] === 'o' ||
//       tmdString[i] === 'u'
//     ) {
//       counts++;
//     }
//   }
//   return counts;
// }

// console.log(vowelReturn(string));

///////////////////////////////////////////////////////////////////////////////////////////////
// You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.
// Examples

// [2, 4, 0, 100, 4, 11, 2602, 36]
// Should return: 11 (the only odd number)

// [160, 3, 1719, 19, 11, 13, -21]
// Should return: 160 (the only even number)
// const arr = [2, 4, 0, 100, 4, 11, 2602, 36];
// const arr2 = [3, 1719, 19, 11, 13, -21, 16];
// let evens = [];
// let odds = [];
// function findOutlier(integers) {
//   for (let i = 0; i < integers.length; i++) {
//     if (integers[i] % 2 == 0) {
//       evens.push(integers[i]);
//     } else {
//       odds.push(integers[i]);
//     }
//   }
//   const evenLength = evens.length;
//   const oddLength = odds.length;

//   if (evenLength > oddLength) {
//     return odds[0];
//   } else {
//     return evens[0];
//   }
// }
// console.log(findOutlier(arr2));

/////////////////////////////////////////////////////////////////////////////////

// Implement a function that adds two numbers together and returns their sum in binary. The conversion can be done before, or after the addition.
// The binary number returned should be a string.

// function addBinary(a, b) {
//   let sum = a + b,
//     binary = '';
//   while (sum > 0) {
//     binary = (sum % 2) + binary;
//     console.log(binary);
//     sum = Math.floor(sum / 2);
//     break;
//   }
//   return binary;
// }
// console.log(addBinary(3, 7)); //Should be 11
// console.log(addBinary(2, 3)); //Should be 111111
// console.log(addBinary(100, 0)); //Should be 1100100

// function decToBin(a, b) {
//   const sum = a + b;
//   return (sum >>> 0).toString(2);
// }
// console.log(decToBin(-3, 1));

// const hex = 1010;
// console.log(parseInt(hex, 2));

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers. No floats or non-positive integers will be passed.

// For example, when an array is passed like [19, 5, 42, 2, 77], the output should be 7.

// const arr2 = [10, 343445353, 3453445, 3453545353453];
// [10, 343445353, 3453445, 3453545353453] should return 3453455.
// const arr = [19, 5, 42, 2, 77];
// let temp;
// let empArr = [];
// const hstr = 'hello';
// Revese
// console.log(arr.reverse());

// for (let i = 0; i < hstr.length; i++) {
//   empArr.push(hstr[i]);
// }
// console.log(empArr);
// const finArr = empArr.reduce((char, revString) => revString + char);
// console.log(finArr);

// const sortedArr = input.sort((a, b) => a - b)
// return sortedArr[0] + sortedArr[1]

// function sumOfLeastNumber(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     for (let j = i + 1; j < arr.length; j++) {
//       if (arr[i] > arr[j]) {
//         temp = arr[i];
//         arr[i] = arr[j];
//         arr[j] = temp;
//       }
//     }
//     empArr.push(arr[i]);
//   }
//   return empArr[0] + empArr[1];
// }

// console.log(sumOfLeastNumber(arr));

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// arr = [1, 2, 4, 5, 6, 5, 58, 48, 5, 4, 15, 4, 84, 874, 89];
// let temp;
// let empArr = [];
// function showSecondLargest(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     for (let j = i + 1; j < arr.length; j++) {
//       if (arr[i] > arr[j]) {
//         temp = arr[i];
//         arr[i] = arr[j];
//         arr[j] = temp;
//       }
//     }
//     empArr.push(arr[i]);
//   }
//   console.log(empArr);
// }
// showSecondLargest(arr);

////////////////////////////////////////////////////////////////////////////////
// Frequency of Number
// const sent = 'Hi People I  love JavaScript!!!';
// const newSent = sent.toLowerCase().match(/[a-z]/g);
// let charMap = {};

// newSent.forEach(char => {
//   charMap[char] ? charMap[char]++ : (charMap[char] = 1);
// });

// console.log(charMap);

/////////////////////////////////////////////////////////////////////////////////////
// Given the triangle of consecutive odd numbers:
//              1
//           3     5
//        7     9    11
//    13    15    17    19
// 21    23    25    27    29

// Calculate the row sums of this triangle from the row index (starting at index 1) e.g.:

// rowSumOddNumbers(1); // 1
// rowSumOddNumbers(2); // 3 + 5 = 8

// let total = 0;
// let sum = 0;
// let firstNum = 0;

// function addRow(input) {
//   for(let i = 0; i < input; i++) {
//       total += i;
//   }

//   firstNum = 2 * total + 1;

//   for(let j = 0; j < input; j++) {
//       sum += firstNum;
//       firstNum += 2;
//   }
//   return sum
// }

// console.log(addRow(3));

////////////////////////////////////////////////////////////////////
// const sent = 'Hi People I  love JavaScript favaScript !!!';
// const newSent = sent.toLowerCase().match(/[a-z]+/g);
// console.log(newSent.reduce((a, b) => (a.length < b.length ? b : a)));

////////////////////////////////////////////////////////////////////
// Given: an array containing hashes of names
// Return: a string formatted as a list of names separated by commas except for the last two names, which should be separated by an ampersand.

// Example:

// list([ {name: 'Bart'}, {name: 'Lisa'}, {name: 'Maggie'} ])
// // returns 'Bart, Lisa & Maggie'

// list([ {name: 'Bart'}, {name: 'Lisa'} ])
// // returns 'Bart & Lisa'

// list([ {name: 'Bart'} ])
// // returns 'Bart'

// list([])
// // returns ''
// const name = [{ name: 'Bart' }, { name: 'Lisa' }, { name: 'Bob' }];

// const res = name.map(item => {
//   return item.name;
// });

// for (let i = 0; i < res.length; i++) {
//   if (res[i] !== res[res.length - 1]) {
//     console.log(res[i]);
//   } else {
//     console.log('&', res[i]);
//   }
// }

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

// findLongest word
// const str = 'Bitcoin take over the !!! world Maybe who knows perhaps Asutosh ';
// function longestWord(sen) {
//   const newStr = sen.toLowerCase().match(/[a-z]+/g);
//   newStr.sort();

//   const sorted = newStr.sort((a, b) => b.length - a.length);

//   const longestWordArr = sorted.filter(
//     word => word.length === sorted[0].length
//   );

//   if (longestWordArr.length === 1) {
//     return longestWordArr[0];
//   } else {
//     return longestWordArr;
//   }
// }

// console.log(longestWord(str));

// function list(names) {
//   return names.reduce(function(prev, current, index, array) {
//     if (index === 0) {
//       return current.name;
//     } else if (index === array.length - 1) {
//       return prev + ' & ' + current.name;
//     } else {
//       return prev + ', ' + current.name;
//     }
//   }, '');
// }
// console.log(list(name));

////////////////////////////////////////////////////////////////////////////////////////

//LONGEST WORD:-

// const str = 'hi i am a developer and superhero and an intern';
// const newStr = str.match(/[a-z]+/g);

// let largest = '';
// for (var i = 0; i < newStr.length; i++) {
//   if (newStr[i].length > largest.length) {
//     largest = newStr[i];
//   }
//   if (newStr[i].length == largest.length) {
//     largest1 = newStr[i];
//   }
// }

// console.log(largest, largest1);
//////////////////////////////////////////////////////////////////////////////////////////
// Given a number, say prod (for product), we search two Fibonacci numbers F(n) and F(n+1) verifying

// F(n) * F(n+1) = prod.

// Your function productFib takes an integer (prod) and returns an array:

// [F(n), F(n+1), true] or {F(n), F(n+1), 1} or (F(n), F(n+1), True)
// depending on the language if F(n) * F(n+1) = prod.

// If you don't find two consecutive F(m) verifying F(m) * F(m+1) = prodyou will return

// [F(m), F(m+1), false] or {F(n), F(n+1), 0} or (F(n), F(n+1), False)
// F(m) being the smallest one such as F(m) * F(m+1) > prod.

// Examples
// productFib(714) # should return [21, 34, true],
// # since F(8) = 21, F(9) = 34 and 714 = 21 * 34

// productFib(800) # should return [34, 55, false],
// # since F(8) = 21, F(9) = 34, F(10) = 55 and 21 34 < 800 < 34 55
// Notes: Not useful here but we can tell how to choose the number n up to which to go: we can use the "golden ratio" phi which is (1 + sqrt(5))/2 knowing that F(n) is asymptotic to: phi^n / sqrt(5). That gives a possible upper bound to n.

//////////////////////////////////////////////////////////////////////////////////////////////////////

// // Define a function with two parameter : A string and an array .
// // Return the position at which the string is present in the array.
// // Eg.:- ( 'racer', ['bacer','cracer','racer','facer']) => 3
// const arr = ['bacer', 'cracer', 'racer', 'facer'];
// function findStr(str, arr) {
//   for (let i = 0; i < arr.length; i++) {
//     if (arr[i] === str) {
//       return i + 1;
//     }
//   }
// }

// console.log(findStr('racer', arr));
//////////////////////////////////////////////////////////////////////////////////////////////////////
// const str = 'bitcoin take over the world maybe who knows perhaps asutosh ';
// const allWord = [];
// let res;
// const newStr = str.match(/[a-z]+/g);

// for (let i = 0; i < newStr.length; i++) {
//     console.log(newStr[i].slice(-1));
//   res =
//     newStr[i][0].toUpperCase() +
//     newStr[i][i.length - 1].toUpperCase() +
//     newStr[i].substring(1);
//   allWord.push(res);
// }
// console.log(allWord.join(' '));
///////////////////////////////////////////////////////

const a = [1, 2, 3, 5, 6, 4, 5, 545, 745, 456, 456, 456, 4];
// let c = [];
// const b = a.map(a => {
//   if (a > 25) {
//     c.push(a);
//   }
// });
// console.log(c);

// const c = a.filter(a => a > 10);
// console.log(c);

// a.forEach(c => console.log(c));
// const c = a.reduce((a, b) => a + b);
// console.log(c);

function binary(arr, x) {
  const a = arr.filter(a => a > x);
  return a;
}
console.log(binary([11, 45, 4, 31, 64, 10], 10));
