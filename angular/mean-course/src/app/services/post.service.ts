import { Injectable } from "@angular/core";
import { PostStruct } from "../model/Post";
import { of } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class PostService {
  // posts: PostStruct[];
  url: string = "http://127.0.0.1:4201";
  constructor(private http: HttpClient) {}

  addPost(postContent: PostStruct) {
    // console.log(postContent);
    return this.http
      .post(`${this.url}/api/posts`, postContent)
      .subscribe((post) => {});
  }

  getPosts() {
    // return of(this.posts);
    return this.http.get<{ message: string; data: PostStruct[] }>(
      `${this.url}/api/posts`
    );
  }
}
