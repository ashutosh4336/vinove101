const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const path = require('path');
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');

// Load ENV variable
dotenv.config({ path: './config/config.env' });

// MongoDB FIle Import
const connectDB = require('./config/db');
// MongoDB connected
connectDB();

// Load Route Files
const bootcamps = require('./routes/bootcamps');
const courses = require('./routes/courses');
const auth = require('./routes/auth');

const app = express();

// Body Parser
app.use(express.json());

// Cookie parser middleware
app.use(cookieParser());

// Dev Logging Middleware
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));
const errorHandler = require('./middleware/error');

// File Upload
app.use(fileupload());

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Mount Routes
app.use('/api/v1/bootcamps', bootcamps);
app.use('/api/v1/courses', courses);
app.use('/api/v1/auth', auth);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;
const server = app.listen(
  PORT,
  console.log(
    `Server Started in Port ${process.env.PORT} on ${process.env.NODE_ENV} mode`
      .yellow.bold
  )
);

// Handle Unhandled Rejection

process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red.bold);
  // Close Server and Exit
  server.close(() => process.exit(1));
});
