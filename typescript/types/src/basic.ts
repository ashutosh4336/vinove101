// Basic

function sum(a: number, b: number, showResult: boolean, message: string) {
  const resultMinus = a - b;
  const resultPlus = a + b;
  if (showResult) return `${message}:  ${resultMinus}`;
  else return `${message}: ${resultPlus}`;
}

const numberOne = 10;
const numberTwo = 18.8;
const printresult = true;
const msg = 'The result is';

const res = sum(numberOne, numberTwo, printresult, msg);
console.log(res);
