// Book Class: Represent a Book
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

// UI CLass: Handle UI Task
class UI {
  static displayBooks() {
    const storeBooks = [
      {
        title: 'BookOne',
        author: 'Jhon Doe',
        isbn: '3538'
      },
      {
        title: 'BookTwo',
        author: 'Shraddha Kapoor',
        isbn: '4336'
      },
      {
        title: 'BookThree',
        author: 'Pinky Panda',
        isbn: '3748'
      }
    ];
    const books = storeBooks;
    // console.log(typeof books);

    books.forEach(book => {
      UI.addBooktoList(book);
    });
  }

  static addBooktoList(book) {
    const list = document.querySelector('#book-list');

    const row = document.createElement('tr');
    row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href="#" class="btn btn-danger btn-sm">X</td>
    `;
    list.appendChild(row);
  }
}

// Store class:Handle Storage

// Event Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks);
// Event Add a Book

document.querySelector('#book-form').addEventListener('submit', e => {
  e.preventDefault();
  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const isbn = document.querySelector('#isbn').value;

  // Instatiate Book
  const book = new Book(title, author, isbn);
  //   console.log(book);
  UI.addBooktoList(book);
});

// Event to Remove a Book
