export interface PostStruct {
  id: string;
  title: string;
  content: string;
}
