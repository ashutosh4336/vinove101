// unknown Type
let userInput: unknown;
let userName: string;
userInput = 5;
userInput = 'Money Heist';

// userName = userInput; //Wrong
// Type 'unknown' is not assignable to type 'string'.ts(2322)
if (typeof userInput === 'string') userName = userInput;

// console.log(userName);

// Void and Never Type
function generateError(message: string, code: number): never {
  throw {
    message,
    code,
  };
}

generateError('Internal Server Error', 500);
