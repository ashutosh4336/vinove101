const chatForm = document.getElementById('chat-form');
const chatMessages = document.querySelector('.chat-messages');
const socket = io();

// Message from server
socket.on('message', message => {
  console.log(message);

  outputMessage(message);

  // ScroolDown
  chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Message Submit
chatForm.addEventListener('submit', e => {
  e.preventDefault();

  //   Get MEssage Value/text
  const message = e.target.elements.msg.value;

  //   Emit a message to server
  socket.emit('chatMessage', message);

  // Clear input
  e.target.elements.msg.value = '';
  e.target.elements.msg.focus();
});

// Output Message to Dom

function outputMessage(message) {
  const div = document.createElement('div');
  div.classList.add('message');
  div.innerHTML = `<p class="meta">Ashutosh4336 <span>9.12PM</span> </p>
                    <p class="text">${message}</p>
  
  `;

  document.querySelector('.chat-messages').appendChild(div);
}
