## TS Configure

### Initialize a Typescript project

```
tsc --init
```

### Exclude files

After compilerOption Brace (Applies to Include also)

```
"exclude": [
    "filename.ts",
    "*.spec.ts"
    "*.dev.ts",
    "**/*.dev.ts"
    "node_modules" // Default Behaviour
]
```

### Include files

If we use Include than we've to add all the files we want to compile

```
"include": [
    "app.ts"
]
```

OR

```
"files" : [
    "app.ts"
]
```

### lib

Initially `"lib"` is commented in `tsconfig.json` which refers to default settings.
Default settings includes all the necessary options and web APIs to work.

We can change the default behaviour by chnaging in `tsconfig.json`

```
"lib": [
      "ES6",
      "DOM",
      "DOM.Iterable",
      "ScriptHost",
      "ES2015.Collection"
    ]
```

### Input and Output file

To set output directory

```
"outDir": "./dist"
```

All source file `.ts` file

```
"rootDir": "./src"
```

To Debug the Typescript code in browser `source` tab we have to enable

```
"sourceMap": true,
```

By which all the typescript files can also be debugged along javascript code.

To remove comments in Javascript file which were in Typescript

```
"removeComments": true
```

If we inly want to check all the project `.ts` files are correct are not we can enable `noEmit` option by which `tsc` will go through the project but don't create `.js` files.

```
"noEmit": true
```

If we want not to create the javascript files if there is an error in `ts` files
we can set

```
"noEmitOnError": true
```

`"noEmitOnError": false` is the default behaviour which creates `.js` file even if there is an error in source `.ts` files.

### Code quality

```
"noUnusedLocals": true,
"noUnusedParameters": true,
"noImplicitReturns": true
```

`noUnusedLocals` option check if there are any unused local variable are are not used.
`noUnusedParameters` lets us know unused function parameter.
`noImplicitReturns` gives an warning when a function return a value in some case and doesn't return in some other case.

To use decorators it is recomended that we enable the option

```
"experimentalDecorators": true
```

It gives supports to decorators.
