// const mA = [
//   [1, 2, 3],
//   [1, 2, 3],
//   [1, 2, 3]
// ];

// const mB = [
//   [9, 8, 11],
//   [9, 8, 11],
//   [9, 8, 11]
// ];
// function matrixMulti(mA, mB) {
//   let result = new Array(mA.length);
//   for (let i = 0; i < result.length; i++) {
//     result[i] = new Array(mB[i].length);
//     for (let j = 0; j < mB[i].length; j++) {
//       result[i][j] = 0;
//       for (let k = 0; k < mA[i].length; k++) {
//         result[i][j] += mA[i][k] * mB[k][j];
//       }
//     }
//   }
//   return result;
// }

// console.log(matrixMulti(mA, mB));

// const str = 'Bitcoin take over the !!! world Maybe who knows perhaps Asutosh ';
// function findShort(input) {
//   const newStr = input.split(' ').sort();

//   const sortedArr = newStr.sort((a, b) => a.length - b.length);

// }

// function longestWord(str) {
//   const wordArr = str.toLowerCase().match(/[a-z0-9]+/g);
//   console.log(wordArr);
//   const sortedSen = wordArr.sort((a, b) => b.length - a.length);

//   const longestWordArr = sortedSen.filter(
//     word => word.length === sortedSen[0].length
//   );
//   longestWordArr.sort();
//   console.log(longestWordArr);
//   if (longestWordArr.length > 1) {
//     return longestWordArr[0];
//   } else {
//     return longestWordArr;
//   }

//   //   console.log(longestWordArr);
// }

// console.log(longestWord(str));

// const num = [
//   1,
//   2,
//   3,
//   4,
//   5,
//   6,
//   8,
//   5,
//   75,
//   454,
//   455,
//   456,
//   457,
//   458,
//   459,
//   460,
//   4,
//   64,
//   6
// ];
// const blankNum = [];
// let prev = 0;

// num.forEach(a => {
//   if (a === 1) {
//     blankNum.push([]);
//   } else if (a - prev !== 1) {
//     blankNum.push([]);
//   }

//   blankNum[blankNum.length - 1].push(a);
//   prev = a;
// });

// const finalAns = blankNum.sort((a, b) => b.length - a.length);
// console.log(finalAns[0]);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// const str = 'Bitcoin take over the !!! world Maybe who knows perhaps Ashutosh';

// function fiveOrMore(str) {
//   const wordArr = str.toLowerCase().match(/[a-z0-9]+/g);

//   const sorted = wordArr.map(word => {
//     if (word.length >= 5) {
//       //   console.log(...word);
//       return [...word].reverse().join('');
//     } else {
//       return word;
//     }
//   });
//   console.log(sorted.join(' '));
// }

// fiveOrMore(str);

/////////////////////////////////////////////////////////////////////////////////////////////////////
// filter_list([1, 2, 'a', 'b']) == [1, 2];
// filter_list([1, 'a', 'b', 0, 15]) == [1, 0, 15];
// filter_list([1, 2, 'aasf', '1', '123', 123]) == [1, 2, 123];

// const rawData = [1, 2, 'aasf', '1', '123', 123];
// const ans = rawData.join(' ')
// console.log(ans.match(/[0-9]+/g));

// for(let i = 0; i < rawData.length; i++) {
//     if(!isNaN(rawData[i])) {
//         console.log(rawData[i]);
//     }
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Find Missing Number
// const arr = [2, 3, 4, 6];

// function findMiss() {
//   for (let i = 0; i < arr.length; i++) {
//     if (arr[i] + 1 !== arr[i + 1]) {
//       return arr[i] + 1;
//     }
//   }
// }

// console.log(findMiss(arr));
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Return the number (count) of vowels in the given string.
// We will consider a, e, i, o, and u as vowels for this Kata.
// The input string will only consist of lower case letters and/or spaces.

// const string = 'abcvhasdhaeiouuu hdhhd';
// const tmdString = string.match(/[a-zA-Z]/g);
// let counts = 0;
// function vowelReturn(string) {
//   for (let i = 0; i < tmdString.length; i++) {
//     if (
//       tmdString[i] === 'a' ||
//       tmdString[i] === 'e' ||
//       tmdString[i] === 'i' ||
//       tmdString[i] === 'o' ||
//       tmdString[i] === 'u'
//     ) {
//       counts++;
//     }
//   }
//   return counts;
// }

// console.log(vowelReturn(string));

///////////////////////////////////////////////////////////////////////////////////////////////
// You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.
// Examples

// [2, 4, 0, 100, 4, 11, 2602, 36]
// Should return: 11 (the only odd number)

// [160, 3, 1719, 19, 11, 13, -21]
// Should return: 160 (the only even number)
// const arr = [2, 4, 0, 100, 4, 11, 2602, 36];
// const arr2 = [3, 1719, 19, 11, 13, -21, 16];
// let evens = [];
// let odds = [];
// function findOutlier(integers) {
//   for (let i = 0; i < integers.length; i++) {
//     if (integers[i] % 2 == 0) {
//       evens.push(integers[i]);
//     } else {
//       odds.push(integers[i]);
//     }
//   }
//   const evenLength = evens.length;
//   const oddLength = odds.length;

//   if (evenLength > oddLength) {
//     return odds[0];
//   } else {
//     return evens[0];
//   }
// }
// console.log(findOutlier(arr2));

/////////////////////////////////////////////////////////////////////////////////

// Implement a function that adds two numbers together and returns their sum in binary. The conversion can be done before, or after the addition.
// The binary number returned should be a string.

// function addBinary(a, b) {
//   let sum = a + b,
//     binary = '';
//   while (sum > 0) {
//     binary = (sum % 2) + binary;
//     sum = Math.floor(sum / 2);
//   }
//   return binary;
// }
// console.log(addBinary(1, 2)); //Should be 11
// console.log(addBinary(2, 3)); //Should be 111111
// console.log(addBinary(100, 0)); //Should be 1100100

// function decToBin(a, b) {
//   const sum = a + b;
//   return (sum >>> 0).toString(2);
// }
// console.log(decToBin(3, 1));
