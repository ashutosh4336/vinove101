// Aliases
type Combineable = number | string;
type resDesc = 'as-number' | 'as-text';

{
  type UserOne = { name: string; age: number };
  const u1: UserOne = { name: 'Ashutosh', age: 21 }; // this works!

  type UserTwo = { name: string; age: number };
  function greet(user: UserTwo) {
    console.log('Hi, I am ' + user.name);
  }

  function isOlder(user: UserTwo, checkAge: number) {
    return checkAge > user.age;
  }
}

// Union Type
function combine(
  inputOne: Combineable,
  inputTwo: Combineable,
  resultConversion: resDesc //Literals Type
) {
  let res;
  if (
    (typeof inputOne === 'number' && typeof inputTwo === 'number') ||
    resultConversion === 'as-number'
  ) {
    res = +inputOne + +inputTwo;
  } else {
    res = inputOne.toString() + ' ' + inputTwo.toString();
  }
  return res;
}
const combineAges = combine(35, 16, 'as-number');
const combineNames = combine('Ashutosh', 'Shruti', 'as-text');
console.log(combineAges);
console.log(combineNames);
