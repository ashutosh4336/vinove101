"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const add = (...num) => {
    return num.reduce((a, b) => {
        return a + b;
    }, 0);
};
const resultAdd = add(4, 56, 8, 9, 6, 5, 6, 6);
console.log(resultAdd);
const printResult = (output) => console.log(output);
const person = {
    name: 'Ashutosh Panda',
    age: 21,
};
const copiedPerson = Object.assign(Object.assign({}, person), { address: 'Noida Delhi India' });
const hobbies = ['sports', 'computers'];
const newHobbies = ['Netflix', 'cooking'];
const finalHobbies = [...hobbies, ...newHobbies];
console.log(finalHobbies);
const [firstHobby, secondHobby] = finalHobbies;
console.log(firstHobby);
console.log(secondHobby);
const { name, age, address } = copiedPerson;
