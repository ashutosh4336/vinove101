const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

// Shoow ErrorFunction
function showError(input, message) {
  const formControl = input.parentElement;
  formControl.className = 'form-control error';
  const small = formControl.querySelector('small');
  small.innerText = message;
}

// Show Success
function showSuccess(input) {
  const formControl = input.parentElement;
  formControl.className = 'form-control success';
}

// Check Email is valid
function checkValidEmail(input) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (re.test(input.value.trim())) {
    showSuccess(input);
  } else {
    showError(input, 'Email is not Valid');
  }
}

// "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"
// Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:
// function checkPasswowdComb(input) {
//   const re = /"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"/;
//   if (re.test(input.value.trim())) {
//     showSuccess(input);
//   } else {
//     showError(input, "Password Policy Doesn't Match");
//   }
// }

// Get Field Name
function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}

// Requied Function
function checkRequied(inputArr) {
  inputArr.forEach(input => {
    // console.log(input.value);
    if (input.value.trim() === '') {
      showError(input, `${getFieldName(input)} Is required`);
    } else {
      showSuccess(input);
    }
  });
}

// check Length
function checkLength(input, min, max) {
  if (input.value.length < min) {
    showError(
      input,
      `${getFieldName(input)} Must be at Least ${min} Characters`
    );
  } else if (input.value.length >= max) {
    showError(
      input,
      `${getFieldName(input)} Must be less than ${max} Characters`
    );
  } else {
    showSuccess(input);
  }
}

//  Password Match Function
function checkPasswordMatch(input1, input2) {
  if (input1.value !== input2.value) {
    showError(input2, "Passwords Don't Match");
  }
}

// Event Listeners
form.addEventListener('submit', e => {
  e.preventDefault();

  checkRequied([username, email, password, password2]);
  checkLength(username, 4, 15);
  checkLength(password, 8, 25);
  checkValidEmail(email);
  checkPasswordMatch(password, password2);
});
