"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Department {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.employees = [];
    }
    describe() {
        console.log(`Department name is : '${this.name}' whose id is ${this.id}`);
    }
    addEmployee(emp) {
        this.employees.push(emp);
    }
    printEmployeeInfo() {
        this.employees.forEach((a) => console.log(`${a}`));
    }
}
class ItDepartment extends Department {
    constructor(id, admins) {
        super(id, 'It Department');
        this.admins = admins;
        this.admins;
    }
    printAdminInfo() {
        console.log(`Admins of IT Department ${this.admins.join()}`);
    }
}
class Finance extends Department {
    constructor(id, reports) {
        super(id, 'Finance Department');
        this.reports = reports;
        this.lastReport = reports[0];
    }
    addReport(text) {
        this.lastReport = text;
        this.reports.push(text);
    }
    getReports() {
        this.reports.forEach((a) => console.log(a));
    }
    addEmployee(name) {
        if (name === 'Ashutosh Panda') {
            return;
        }
        this.employees.push(name);
    }
}
const depOne = new Department(0o1, 'Development');
depOne.addEmployee('Ashutosh Panda');
depOne.addEmployee('Shruti');
const itDep = new ItDepartment(0o2, ['Swapna Jha', 'Ajay Tandon']);
const finDep = new Finance(0o3, []);
finDep.describe();
finDep.addReport('Q4 was awesome');
finDep.getReports();
finDep.addEmployee('Shraddha Kapoor');
finDep.printEmployeeInfo();
