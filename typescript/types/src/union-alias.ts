// console.log('Union Types');

// Alias
type numStr = number | string;
type literalType = 'as-number' | 'as-text';

/*
Type aliases can be used to "create" custom types.
It is not limited to storing union types though.
we can also provide an alias to a (possibly complex) object type.

*/
type User = { name: string; age: number };
const u1: User = { name: 'Max', age: 30 };
const u2: User = { name: 'Ashutosh', age: 21 };

type Product = { title: string; price: number };
const p1: Product = { title: 'A Book', price: 12.99 };

function combine(
  a: numStr,
  b: numStr,
  resultConversion: literalType // literal type
) {
  let res;
  if (
    (typeof a === 'number' && typeof b === 'number') ||
    resultConversion === 'as-number'
  )
    res = +a + +b;
  else res = a.toString() + ' ' + b.toString();

  // if (resultConversion === 'as-number') return +res;
  // else return res.toString();

  return res;
}

console.log(combine(56, 78, 'as-number'));
console.log(combine('10', '48', 'as-number'));
console.log(combine('Ashutosh', 'Panda', 'as-text'));
