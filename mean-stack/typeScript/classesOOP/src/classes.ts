// Simple Class Demo
abstract class Department {
  static fiscalYear = 2020;
  // private name: string = 'Default';
  // private readonly id: number = 'Default';

  protected employees: string[] = [];

  constructor(private name: string, protected readonly id: number) {}

  // Static Method
  static createEmployee(name: string) {
    return { name: name };
  }

  abstract describe(this: Department): void;
  //  {
  //   console.log('Department Name is : ' + this.name + 'With Id : ' + this.id);
  // }

  addEmployee(empName: string) {
    this.employees.push(empName);
  }

  printEmployee() {
    console.log(this.employees);
  }
}
/*
  --> Static Method is something that 
      We can call the Method
      Without instanting the class
  --> Static Method or Properties
      can't be accessed by other 
      non-Static Methods and Constructor
*/
// Static Method Testing
// const empOne = Department.createEmployee('Ashutosh');
// console.log(empOne, Department.fiscalYear);

/*
  --> abstract Classes can't be instantatied
*/
/*
const mean = new Department('MEAN Department', 13);
// mean.describe();
mean.addEmployee('Ahsutosh Panda');
mean.addEmployee('Shruti Gupta');
mean.printEmployee();
*/

// IT Department extends Demo
class Itdeaprtment extends Department {
  constructor(id: number, public admins: string[]) {
    super('It Department', id);
    this.admins = admins;
  }

  describe() {
    console.log('It Departments Admins are : ' + this.admins);
  }
}

const itDep = new Itdeaprtment(0o1, ['Dimple Tyagi', 'Ajya Tandon']);
itDep.addEmployee('Parvesh Agrawal');
itDep.describe();
// console.log(itDep);

// Accounting Department Inheritance Demo
class AccountingDep extends Department {
  // Getters & Setters
  private lastReport: string;
  private static instance: AccountingDep;

  // Getters
  get recentReport() {
    if (this.lastReport) return this.lastReport;
    throw new Error('No Reports Found');
  }

  // Setters
  set recentReport(value: string) {
    if (!value) throw new Error('Please Provide a Valid Report');

    this.addReports(value);
  }

  private constructor(id: number, public reports: string[]) {
    super('Accounting Department', id);
    this.lastReport = reports[0];
  }

  static getInstance() {
    if (this.instance) return this.instance;
    return (this.instance = new AccountingDep(0o02, []));
  }

  describe() {
    console.log('Accounting Department - ID : ' + this.id);
  }

  addReports(text: string) {
    this.reports.push(text);
    this.lastReport = text;
  }
  getReport() {
    console.log(this.reports);
  }

  addEmployee(name: string) {
    if (name === 'Dimple Tyagi') return;
    this.employees.push(name);
  }
  printEmployee() {
    console.log(this.employees);
  }
}

/*
    In Private Constructor we cant instantiate
    the class out side of the class  
*/
// const accDep = new AccountingDep(0o02, []);
const accDep = AccountingDep.getInstance();

accDep.addEmployee('Ajya Pattanik');
accDep.addEmployee('Dimple Tyagi');

// Getter Testing
// First Providing a Report than Printing it on Screen
accDep.addReports(
  'The Company has Performed Well Inspite of the Covid-19 Pandemic'
);
// console.log(accDep.recentReport);

// Setter Testing
accDep.recentReport = 'Q4 Results are Dispointing';

accDep.addReports('However the total Turnover Decreased by 2%');
accDep.getReport();
// accDep.printEmployee();

// Overriding Describe Method
accDep.describe();

console.log(accDep);
