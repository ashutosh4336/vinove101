"use strict";
const add = (a, b = 5) => a + b;
const printOutput = (output) => console.log(output);
const hobbies = ['Sports', 'Cooking', 'Computers', 'Travelling'];
const activeHobbies = ['Netflix'];
activeHobbies.unshift(...hobbies);
console.log(activeHobbies);
const person = {
    name: 'Ashutosh Panda',
    age: 21,
    userName: 'ashutosh4336',
    phoneNumber: '8763076463',
    employementStatus: 'Junior Software Developer',
};
const newPerson = Object.assign(Object.assign({}, person), { address: 'Noida Sector 62' });
console.log(newPerson);
console.log(person);
