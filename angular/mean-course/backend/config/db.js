const mongoose = require("mongoose");

const connectDB = async () => {
  const conn = await mongoose.connect(
    "mongodb+srv://ashutosh:ashutosh123@ashutosh4336-ewcho.mongodb.net/mean-app?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    }
  );
  console.log(`MongoDB Connected ${conn.connection.host}`.cyan.underline.bold);
};

module.exports = connectDB;
