const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

// Check Required for All Field
function checkRequired(inputArr) {
  inputArr.forEach(input => {
    // console.log(input.value);
    if (input.value.trim() === '') {
      showError(input, `${getFieldName(input)} is Required`);
    } else {
      showSuccess(input);
    }
  });
}

// Get Field Name
function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}

// SuhowError Function (Input Error Message)
function showError(input, message) {
  const formControl = input.parentElement;
  formControl.className = 'form-control error';
  const small = formControl.querySelector('small');
  small.innerText = message;
}

// showSuccess Function (Input Success)
function showSuccess(input) {
  const formControl = input.parentElement;
  formControl.className = 'form-control success';
}

// Validate Email
function checkEmail(input) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (re.test(input.value)) {
    showSuccess(input);
  } else {
    showError(input, 'Email is Not Valid');
  }
}
// CheckPassword
function checkPassword(input) {
  const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
  if (re.test(input.value.trim())) {
    showSuccess(input);
  } else {
    showError(input, 'Password is Not Valid');
  }
}

// Passwor Match
function matchPassword(input1, input2) {
  if (input1.value !== input2.value) {
    showError(input2, 'Confirm Password Must be same as Password');
  } else {
    showSuccess(input2);
  }
}

// checkLength Function

function checkLength(input, min, max) {
  if (input.value.length < min) {
    showError(input, `${getFieldName(input)} Must be atlease ${min} Character`);
  } else if (input.value.length > max) {
    showError(
      input,
      `${getFieldName(input)} Must be less than ${max} Character`
    );
  } else {
    showSuccess(input);
  }
}

form.addEventListener('submit', e => {
  e.preventDefault();

  checkRequired([username, email, password, password2]);
  checkLength(username, 3, 15);
  checkPassword(password);
  checkPassword(password2);
  matchPassword(password, password2);
  checkEmail(email);
});

//------------------------------------------------------//
// Traditional Validation

//   if (username.value === '') {
//     showError(username, 'Username is Required');
//   } else {
//     showSuccess(username);
//   }
//   if (email.value === '') {
//     showError(email, 'Email is Required');
//   } else if (!isValidEmail(email.value)) {
//     showError(email, 'Email is Not Valid');
//   } else {
//     showSuccess(email);
//   }
//   if (password.value === '') {
//     showError(password, 'Password is Required');
//   } else {
//     showSuccess(password);
//   }
//   if (password2.value === '') {
//     showError(password2, 'Confirm Password is Required');
//   } else {
//     showSuccess(password2);
//   }
