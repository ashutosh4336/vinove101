import { Component } from '@angular/core';
// import arrayWords from '../utils/words';
import arrayCountries from '../utils/words';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'word-generator';

  words = '';
  limit = 10;

  handleSlideChange(newLimit: number) {
    this.limit = newLimit;
  }

  generate() {
    // this.words = arrayWords.slice(0, this.limit).join(' ');
    // console.log(this.words);
    const countriesArr = arrayCountries.map((el) => el.code);
    // console.log(countriesArr);

    // this.words = countriesArr
    //   .slice(0, this.limit)
    //   .join(' ')
    //   .replace(/ /g, ', ');

    const shuffled = countriesArr.sort(() => 0.5 - Math.random());
    this.words = shuffled.slice(0, this.limit).join(' ').replace(/ /g, ', ');
  }
}
