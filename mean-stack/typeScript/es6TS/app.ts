// const add = (n1: number, n2: number) => n1 + n2;

// const printOutput: (s: number | string) => void = (output) => {
//   console.log(output);
// };

// // printOutput(add(5, 6));

// const someBtn = document.querySelector('button');

// if (someBtn) {
//   someBtn.addEventListener('click', (event) =>
//     console.log(
//       'Clicked at Postion [X, Y]: ' +
//         '(' +
//         event.offsetX +
//         ' ' +
//         event.offsetY +
//         ')'
//     )
//   );
// }

const add = (a: number, b: number = 5) =>
  a + b; /* Default Parameter always Comes in Last Place */
const printOutput: (a: number | string) => void = (output) =>
  console.log(output);

// printOutput(add(5));

const hobbies: string[] = ['Sports', 'Cooking', 'Computers', 'Travelling'];

const activeHobbies: string[] = ['Netflix'];
activeHobbies.unshift(...hobbies);

console.log(activeHobbies);

const person = {
  name: 'Ashutosh Panda',
  age: 21,
  userName: 'ashutosh4336',
  phoneNumber: '8763076463',
  employementStatus: 'Junior Software Developer',
};
const newPerson = { ...person, address: 'Noida Sector 62' };
console.log(newPerson);
console.log(person);
