import { FormGroup } from '@angular/forms';

export function PasswordCheck(controlName: string, CompareControlName: string) {
  return (formGroup: FormGroup) => {
    const password = formGroup.controls[controlName];
    const confPassword = formGroup.controls[CompareControlName];

    if (password.value === confPassword.value) {
      confPassword.setErrors(null);
    } else {
      confPassword.setErrors({ mustMatch: true });
    }
  };
}
