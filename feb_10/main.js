// var name = 'Vinove';

// function abc() {
//   var data = 'Value Coders';
//   function s() {
//     console.log(data);
//   }
//   return s;
// }

// var empCode = abc();
// empCode();

const companies = [
  { name: 'Company One', category: 'Finance', start: 1981, end: 2004 },
  { name: 'Company Two', category: 'Retail', start: 1992, end: 2008 },
  { name: 'Company Three', category: 'Auto', start: 1999, end: 2007 },
  { name: 'Company Four', category: 'Retail', start: 1989, end: 2010 },
  { name: 'Company Five', category: 'Technology', start: 2009, end: 2014 },
  { name: 'Company Six', category: 'Finance', start: 1987, end: 2010 },
  { name: 'Company Seven', category: 'Auto', start: 1986, end: 1996 },
  { name: 'Company Eight', category: 'Technology', start: 2011, end: 2016 },
  { name: 'Company Nine', category: 'Retail', start: 1981, end: 1989 }
];

const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];

// console.log(ages);

// forEach = loop through the array
// Doesn't return anyting

// for (let i = 0; i < companies.length; i++) console.log(companies[i]);
// var name = 'Vinove';

// function abc() {
//   var data = 'Value Coders';
//   function s() {
//     console.log(data);
//   }
//   return s;
// }

// var empCode = abc();

// empCode();
// es.forEach((company, index, all) => console.log(company.name));

// // Filtervar name = 'Vinove';

// function abc() {
//   var data = 'Value Coders';
//   function s() {
//     console.log(data);
//   }
//   return s;
// }

// var empCode = abc();

// empCode();
// let canDrink = [];

// for (let i = 0; i < ages.length; i++) {
//   if (ages[i] > 18) {
//     canDrink.push(ages[i]);
//   }
// }
// console.log(canDrink.length);
// const canDrink = ages.filter(age => {
//   if (age >= 18) {
//     // console.log(age);
//     return true;
//   }
// });

// const canDrink = ages.filter(age => age >= 18);

// let myAge = 'ashutosh';
// let newAge = myAge.split('');
// console.log(newAge);

// let something = [1, 2, 3, 4, 5, 4, 5, 5];
// console.log(typeof something);

// const ashutosh = 010;
// console.log(typeof ashutosh);

const retailCompanies = companies.filter(
  company => company.category === 'Retail'
);

// console.log(retailCompanies);

const eightiesCompanies = companies.filter(
  company => company.start >= 1980 && company.start < 1990
);
// console.log(eightiesCompanies);

// Get Companines that lassted 10 years

const lastedTenyears = companies.filter(
  company => company.end - company.start >= 10
);
// console.log(lastedTenyears);

// Map
// Create Array of Company names
const companyName = companies.map(
  company =>
    `${company.name} started in ${company.start} ended in ${company.end}`
);
// console.log(companyName);
const ageSq = ages.map(age => Math.sqrt(age)).map(age => age * 2);

// console.log(ageSq);

// const sortedCompany = companies.sort((comapny1, comapny2) => {
//   if (comapny1.start > comapny2.start) {
//     return 1;
//   } else {
//     return -1;
//   }
// });

const sortedCompany = companies.sort((a, b) => (a.start > b.start ? 1 : -1));

// console.log(sortedCompany);

// const sortAges = ages.sort((a, b) => a - b);

// const sortAges = ages.sort((a, b) => b - a);
// // console.log(sortAges);

// Clouser

// var name = 'Vinove';

// function abc() {
//   var childName = 'ValueCoders';
//   function a() {
//     console.log(childName);
//   }
//   a();
//   return name;
// }

// var empCode = abc();
// console.log(empCode);

const object1 = {
  firstName: 'Ashutosh',
  lastName: 'Panda',
  age: 21
};

const object2 = {
  age: 21,
  occup: 'Unemplyeed'
};

const object3 = {
  Status: 'NotMarried'
};

// let object3 = { ...object1, ...object2 };
// let object3 = Object.assign(object1, object2);

let object4 = { ...object1, ...object2, ...object3 };
// console.log(object4);
