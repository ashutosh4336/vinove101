// // // --------------------------------- Number 1 -------------------------------------------//

// // //  - Number should be greater than 4 & without duplicate entries.
// // //  - Addition of all the elements in the array.
// // //  - Output should be print on <P> tag.

// const data = [3, 47, 8, 9, 345, 907, 5, 5, 8, 4, 345];

// let b = new Set(data);
// b = [...b];
// // console.log(b);

// let c = [];
// const newData = b.map(el => {
//   if (el > 4) {
//     c.push(el);
//   }
// });
// console.log(c);

// const sumOfArr = c.reduce((a, b) => a + b);
// console.log(sumOfArr);

// const pTagOne = document.createElement('p');
// pTagOne.innerHTML = `First Answer ${sumOfArr}`;
// document.body.appendChild(pTagOne);
// --------------------------------- Number 2 -------------------------------------------//

// Running Instruction :
// kindly Comment all other Code (Number 1,2,4)
// navigate to the containing folder
// in Trminal "node script.js"

const numberX = [2, 3, 56, 34, 83, 7, 5, 6, 9];
const dataX = [4, 56, 3, 4, 78, 94, 7, 5, 6];

let tsArr = [];

for (let i = 0; i < numberX.length; i++) {
  tsArr.push(numberX[i]);
}
for (let i = 0; i < dataX.length; i++) {
  tsArr.unshift(dataX[i]);
}
console.log(tsArr);

// let funArr = new Set(tsArr);
// funArr = [...funArr];
// // console.log(funArr);

const newIdeaArr = tsArr.filter((a, b) => tsArr.indexOf(a) === b);
console.log(newIdeaArr);

const tempArr = [...numberX, ...dataX];

let finArr = new Set(tempArr);
finArr = [...finArr];
console.log(finArr);
let isNotPrime = [];

let uniqNotPrime;
let uniqNotPrimeArr;
let myArray = [];
function findPrime(input) {
  for (let k = 0; k < input.length; k++) {
    if (input[k] === 2) {
      myArray.push(input[k]);
    }
    for (let i = 2; i < input[k]; i++) {
      if (input[k] % i === 0) {
        isNotPrime.push(input[k]);
      }
    }
  }

  uniqNotPrime = new Set(isNotPrime);
  uniqNotPrimeArr = [...uniqNotPrime];

  myArray = finArr.filter(function(el) {
    return uniqNotPrimeArr.indexOf(el) < 0;
  });
  return [uniqNotPrimeArr, myArray];
}

const outputTwo = findPrime(finArr);
console.log(outputTwo);

// --------------------------------- Number 3 -------------------------------------------//

// Running Instruction :
// kindly Comment all other Code (Number 1,2,4)
// navigate to the containing folder
// in Trminal "node script.js"

// let cu = 0;
// let firstEl = 1;
// let secondEl;
// let cuArr = [];
// let cuObj = {};
// function fibonacci(input) {
//   for (let i = 0; i < input; i++) {
//     // console.log(cu);
//     cuArr.push(cu);
//     secondEl = cu + firstEl;
//     cu = firstEl;
//     firstEl = secondEl;
//   }

//   // console.log(cuArr);
//   const resObject = cuArr.map(el => {
//     cuObj[el] ? cuObj[el] : (cuObj[el] = el);
//   });
//   return cuObj;
// }

// const outputNumThree = fibonacci(10);
// console.log(outputNumThree);

// --------------------------------- Number 4 -------------------------------------------//

// const unSortedArr = [4, 96, 34, 5, 65, 203, 87, 345];
// let temp;
// let empArr = [];

// function sortArr(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     for (let j = i + 1; j < arr.length; j++) {
//       if (arr[i] > arr[j]) {
//         temp = arr[i];
//         arr[i] = arr[j];
//         arr[j] = temp;
//       }
//     }
//     empArr.push(arr[i]);
//   }
//   return empArr;
// }
// const numFourOutput = sortArr(unSortedArr);
// console.log(numFourOutput);

// const pTagFour = document.createElement('p');
// pTagFour.innerHTML = `Number Four Answer : [${numFourOutput}]`;

// document.body.appendChild(pTagFour);
