const mongoose = require("mongoose");

const PostSchema = new mongoose.Schema({
  id: {
    type: String,
  },
  title: {
    type: String,
    require: [true, "Please add a Title"],
    trim: true,
    maxlength: [50, "Title can't be More than 50 Characters"],
  },

  content: {
    type: String,
    require: [true, "Please add Content"],
    maxlength: [500, "Title can't be More than 500 Characters"],
  },
});

module.exports = mongoose.model("Post", PostSchema);
