const express = require('express');
const dotenv = require('dotenv');
const colors = require('colors');
const morgan = require('morgan');
const path = require('path');
const http = require('http');
const socketio = require('socket.io');
// Load Env Variables
dotenv.config({ path: './config/config.env' });

const app = express();
const server = http.createServer(app);

const io = socketio(server);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Dev Logging Middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// run when a client connect
io.on('connect', socket => {
  //   console.log('new Web Socket Connection');

  //   Welcome Current User
  socket.emit('message', 'welcome to LiveChatRoom');

  // Boardcast when a User Connect
  socket.broadcast.emit('message', 'A User has Joined the Chat');

  //   Run when a Client Disconnect
  socket.on('disconnect', () => {
    io.emit('message', 'A User has Left the Chart');
  });

  //   Listen for Chat Message
  socket.on('chatMessage', msg => {
    // console.log(msg);

    io.emit('message', msg);
  });

  //   io.emit() // All the Clients in general
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () =>
  console.log(
    `Server Started in ${process.env.NODE_ENV} mode on ${PORT}`.cyan.bold
  )
);
