## Angular [LearnCodeOnline](https://web.learncodeonline.in/)

Install `Angular`

```
Linux - sudo npm install -g @angular/cli
Mac - sudo npm install -g @angular/cli
Windows - npm install -g @angular/cli
```

### Create a new angular app

```
ng new my-app
```

In the place of `my-app` add your desired name.

### Run the application

```
cd my-app
ng serve --open
```

To run the application first move in to the app directory by `cd` command than give the provided command to run the development server on `PORT 4200` and the `open` flag is to directly open into your default browser.

### CLI Refernce

To get help

```
ng help
```

#### Some important Commands

- `new` Creates a new workspace and an initial Angular app. The alias for new is `n`.

        eg ng n my-app

- `generate` Generates and/or modifies files based on a schematic. The alias for new is `g`.

        eg ng g sub-commands (appShell, application, class, module, component, guard, service, directive, enum, pipe, serviceworker)

- `add` Adds support for an external library to your project. There is no alias for `add`.

  eg - ng add <collection> [options]

  - To add third party support for app like to add Progressive Web App to your application `ng add @angular/pwa`

* `build` Compiles an Angular app into an output directory named dist/ at the given output path. Must be executed from within a workspace directory. The alias for build is `b`

        eg - ng build <project> [options]

### Angular file structure

```
tsconfig.json
tsconfig.app.json
tsconfig.spec.json
tslint.json
```

Are the file which deals with `TypeScript` configuration for the project.

```
package.json
```

package.json is the file where all the information regarding your application. Information like app name, version, scripts, dependencies, devDependencies, license Etc.

```
angular.json
```

In this file all the configuration of the application resides. teh configuraton of the app can the derived by `ng config` command which outputs this file.

`.editorconfig` file holds the Code editor configuration for the poroject.

`node_modules` is the directory where all the dependencies file is stored.

`e2e` End to End Testing

```
src
```

all the application file
