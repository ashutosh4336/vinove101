const rowForm = document.getElementById('parent-form-row');

const insertEmail = document.createElement('div');

insertEmail.className = 'form-group col-md-6';
insertEmail.id = 'creating-Element';

const emailLabel = document.createElement('label');
emailLabel.innerHTML = 'Email';
// const labelTextNode = document.createTextNode('Email');
// const labelWithText = emailLabel.appendChild(labelTextNode);

// console.log(labelWithText);

// Email Input Field Create
const emailInput = document.createElement('input');
emailInput.className = 'form-control';
emailInput.id = 'email';
emailInput.placeholder = 'Enter Email';
emailInput.setAttribute('type', 'email');

insertEmail.appendChild(emailLabel);
insertEmail.appendChild(emailInput);

// console.log(insertEmail);

rowForm.appendChild(insertEmail);

// Radio Button
const innerDiv = document.createElement('div');
innerDiv.className = 'form-group col-md-6';
// const nextDiv = document.createElement('div');
// nextDiv.className = 'form-row';
// innerDiv.id = 'radio-btn-div';
const firstradioBtn = document.createElement('input');
const secondradioBtn = document.createElement('input');
const thirdradioBtn = document.createElement('input');
const firstLabel = document.createElement('label');
const secondLabel = document.createElement('label');
const thirdLabel = document.createElement('label');

firstradioBtn.className = 'form-check-input';
firstradioBtn.id = 'second-group-firstRadio';
firstradioBtn.setAttribute('type', 'radio');
firstradioBtn.setAttribute('name', 'b');

secondradioBtn.className = 'form-check-input';
secondradioBtn.id = 'second-group-secondRadio';
secondradioBtn.setAttribute('type', 'radio');
secondradioBtn.setAttribute('name', 'b');

thirdradioBtn.className = 'form-check-input';
thirdradioBtn.id = 'second-group-thirdRadio';
thirdradioBtn.setAttribute('type', 'radio');
thirdradioBtn.setAttribute('name', 'b');

firstLabel.className = 'form-check-label';
secondLabel.className = 'form-check-label';
thirdLabel.className = 'form-check-label';

firstLabel.textContent = 'Married';
secondLabel.textContent = 'Unmarried';
thirdLabel.textContent = 'Divorced';

const eachItemOne = document.createElement('div');
eachItemOne.className = 'form-check form-check-inline';
eachItemOne.id = 'group-b-first';

const eachItemTwo = document.createElement('div');
eachItemTwo.className = 'form-check form-check-inline';
eachItemTwo.id = 'group-b-second';

const eachItemThree = document.createElement('div');
eachItemThree.className = 'form-check form-check-inline';
eachItemThree.id = 'group-b-third';

eachItemOne.appendChild(firstradioBtn);
eachItemOne.appendChild(firstLabel);

eachItemTwo.appendChild(secondradioBtn);
eachItemTwo.appendChild(secondLabel);

eachItemThree.appendChild(thirdradioBtn);
eachItemThree.appendChild(thirdLabel);

innerDiv.appendChild(eachItemOne);
innerDiv.appendChild(eachItemTwo);
innerDiv.appendChild(eachItemThree);

const parentDiv = document.getElementById('second-radio-group-b');

parentDiv.appendChild(innerDiv);

// Table Insert Data
const table = document.getElementById('table');
const form = document.getElementById('main-form');

form.addEventListener('submit', e => {
  e.preventDefault();

  const newRow = table.insertRow(table.length);
  const cellOne = newRow.insertCell(0);
  const cellTwo = newRow.insertCell(1);
  const cellThree = newRow.insertCell(2);
  const cellFour = newRow.insertCell(3);

  const firstName = document.getElementById('firstName').value;
  const lastName = document.getElementById('lastName').value;
  const age = document.getElementById('age').value;
  const email = document.getElementById('email').value;

  cellOne.innerHTML = firstName;
  cellTwo.innerHTML = lastName;
  cellThree.innerHTML = age;
  cellFour.innerHTML = email;
});
