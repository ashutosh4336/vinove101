// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBB06p0Pc4AoiAOqGrU_hMUyO95CZImdAI',
    authDomain: 'githubfinder-538a2.firebaseapp.com',
    databaseURL: 'https://githubfinder-538a2.firebaseio.com',
    projectId: 'githubfinder-538a2',
    storageBucket: 'githubfinder-538a2.appspot.com',
    messagingSenderId: '993103166741',
    appId: '1:993103166741:web:b52d061a797cf39630fd1c',
    measurementId: 'G-J4G77X6XHX',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

// <!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->
// <script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-analytics.js"></script>

// <script>
//   // Your web app's Firebase configuration
//   var firebaseConfig = {
//     apiKey: "AIzaSyBB06p0Pc4AoiAOqGrU_hMUyO95CZImdAI",
//     authDomain: "githubfinder-538a2.firebaseapp.com",
//     databaseURL: "https://githubfinder-538a2.firebaseio.com",
//     projectId: "githubfinder-538a2",
//     storageBucket: "githubfinder-538a2.appspot.com",
//     messagingSenderId: "993103166741",
//     appId: "1:993103166741:web:b52d061a797cf39630fd1c",
//     measurementId: "G-J4G77X6XHX"
//   };
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
//   firebase.analytics();
// </script>
