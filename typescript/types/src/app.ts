// "unknown" type and "never" type

let userInput: unknown;
let userName: string;

userInput = 10;
userInput = 'Ashutosh';

// userName = userInput; // Type 'unknown' is not assignable to type 'string'.
// if userInput was unknown type than the above line of code would be correct

if (typeof userInput === 'string') {
  userName = userInput;
  console.log(userName);
}

function generateError(message: string, code: number): never {
  throw { errorMessage: message, errorCode: code };
}

generateError('Bad request', 400);
