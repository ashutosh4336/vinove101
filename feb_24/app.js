// var a = 'Something';
// console.log(this.a);
// this.a = 'Something Else';
// console.log(a);

// function test() {
//   console.log(myVar);
//   console.log(this.myVar);
// }
// var myVar = 123;
// test();

// const val1 = 10;
// const val2 = 5;

// if (val1 + val2 < 15) {
//   console.log('First IF');
// } else if (val1 === '10') {
//   console.log('Second Else IF');
// } else if (val2 > 4) {
//   console.log('Third loop');
// } else {
//   console.log('Fourth');
// }

// var name = 'Chandler'
// var name = 'Monica';

// var undefined = 'something';
// console.log(undefined);

// function doSome() {}
// doSome.prototype.foo = 'bar';

// var doinstance = new doSome();

// doinstance.prop = 'some value';

// console.log(doinstance.prop); // Some Value
// console.log(doinstance.foo); // bar
// console.log(doSome.prop); // undefined
// console.log(doSome.foo); // undefined
// console.log(doSome.prototype.foo); //bar
// console.log(doSome.prototype.prop); // undefined

// var a = { a: 1 };

// var b = Object.create(a);
// console.log(b.a); //1
// console.log(this.a); //undefined

// var c = Object.create(null);
// console.log(c.a); //null

// c.a = 4;

// var d = Object.create(c);
// console.log(d.a);

// var e = Object.create(d);
// console.log(e.a);

// var a = { a: 1 };
// var b = Object.create(a);

// console.log(a.a);
// console.log(b.a);

// b.a = 5;
// console.log(a.a);
// console.log(b.a);

// delete b.a;
// console.log(a.a);
// console.log(b.a);

// delete a.a;
// console.log(a.a);
// console.log(b.a);
// 'use strict';
// let a = (b = 10);
// console.log(a, b);

// class Polygon {
//   constructor(height, width) {
//     this.height = height;
//     this.width = width;
//   }

//   get area() {
//     return this.height * this.width;
//   }
// }

// class Square extends Polygon {
//   constructor(sideLength) {
//     super(sideLength, sideLength);
//   }
//   get area() {
//     return this.height * this.width;
//   }
//   set sideLength(newLength) {
//     this.height = newLength;
//     this.width = newLength;
//   }
// }

// var square = new Square(2);
// var polygon = new Polygon(4, 8);
// console.log(polygon);
// console.log(polygon.area);

// console.log(square);

// console.log(square.area);

// function multiply(a, b) {
//   b = typeof b !== undefined ? b : 1;
//   console.log(a * b);
//   console.log(typeof b);
// }

// multiply(5, 2); // 10
// multiply(5); // NaN

// function append(value, array = []) {
//   array.push(value);
//   console.log(array);
// }

// append(1, [2, 6]);

// ######################################################### //

// let string = '12de3';
// console.log(string.length);

// function sumOfDigit(str) {
//   const empArr = [];
//   const newString = str.replace(/[^0-9]/gi, ' ').split(' ');

//   let filtered = newString.filter(el => {
//     return el;
//   });

//   for (let i = 0; i < filtered.length; i++) {
//     empArr.push(parseInt(filtered[i]));
//   }
//   console.log(typeof empArr[1]);

//   const sumArr = empArr.reduce((a, b) => a + b);
//   console.log(sumArr);
// }

// sumOfDigit(string);
// ######################################################################################## //

// let abc = 'ahdf45fd  f14!!fHHrf52';
// let arr = [];
// let newStr = abc.match(/\d+/g);
// console.log(newStr);

// for (let i = 0; i < newStr.length; i++) {
//   arr.push(parseInt(newStr[i]));
// }

// const res = arr.reduce((a, b) => a + b);
// console.log(res);

// ######################################################################################## //

let object = { a: [1, 2, 3], b: [2, 3], c: [1, 3, 5] };

let objectArr = [];

for (let key in object) {
  objectArr.push(...object[key]);
}

let finalObject = {};

objectArr.forEach(el => {
  if (!finalObject[el]) {
    finalObject[el] = [];
  }
});

for (key in finalObject) {
  for (childkey in object) {
    const element = object[childkey].find(el => el == key);
    if (element) finalObject[key].push(childkey);
  }
}

console.log(finalObject);
