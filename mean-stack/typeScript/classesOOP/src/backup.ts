// // class Department {
// //   nameofDeaprtment: string;
// //   headofDdeaprt: string;

// //   constructor(name: string, head: string) {
// //     this.nameofDeaprtment = name;
// //     this.headofDdeaprt = head;
// //   }

// //   describe(this: Department) {
// //     // console.log(`${this.nameofDeaprtment}'s Head is ${this.headofDdeaprt}`);
// //     return `${this.nameofDeaprtment}'s Head is ${this.headofDdeaprt}`;
// //   }
// // }

// // // const accDepart = new Department('Accounting', 'Raghu Ram Rajan');
// // const adminDepart = new Department('Admin Department', 'Swapna Jha');
// // const adminDepartCopy = { describe: adminDepart.describe() };

// // // console.log(accDepart);
// // // console.log(adminDepart);

// // // const fullSen = adminDepart.describe();
// // // console.log(fullSen);
// // // console.log(adminDepartCopy);

// // // console.log(adminDepart.describe());

// class Department {
//     static createEmployee(arg0: string) {
//         throw new Error('Method not implemented.');
//     }
//     // private readonly name: string;
//     // private id: number;
//     protected employees: string[] = [];

//     /*
//       shortHand Initilization with public and Private keyword
//       we can directly pass the required fields in constructor
//       parameter in the constructor body it is not even
//       Required to redeclare with this keyword
//   -----------------------------------------------------------------
//       readonly is only for typescript
//       javascript doesn't understand readonly
//     */

//     constructor(public name: string, private readonly id: number) {
//         // this.name = name;
//         // this.id = id;
//     }

//     createEmployee(name: string) {
//         return { name: name };
//     }

//     describe(this: Department) {
//         console.log(`Department (${this.id}): ` + this.name);
//     }

//     addEmployee(...emp: string[]) {
//         this.employees.push(...emp);
//     }

//     printEmployee() {
//         console.log(this.employees.length);
//         console.log(this.employees);
//     }
// }

// class itDepartment extends Department {
//     constructor(id: number, public admins: string[]) {
//         super('IT Department', id);
//     }
// }
// const empOne = Department.createEmployee('Max');
// console.log(empOne);

// const account = new Department('Accounting', 1);
// // account.describe();

// // account.printEmployee();
// account.addEmployee('Nilesh Mishra', 'Rita Mallik', 'Rahul Srivastav');

// const vinoveNoida = new itDepartment(0o2, ['Swapna Jha', 'Tanwar Ali']);
// vinoveNoida.addEmployee('Vishar Atri', 'Dimple Tyagi');
// // console.log(vinoveNoida);

// // vinoveNoida.printEmployee();

// // Adding Employee (Element) directly assigning

// /*
//     When Used the Private key word with the
//     employees Property We can't assign directloy
//     value to the employees Array from outside of the class
//     ------------------------------------------------------
//     public key word is also there which is default behaviour
//     name: string === public name: string
// */

// // // account.employees[0] = 'Nitish Kumar';
// // account.employees.unshift('Dimple Tyagi');
// // account.employees.push('Divya Tyagi');

// // account.printEmployee();

// class HrDepartment extends Department {
//     private lastReport: string;

//     get prevReport() {
//         if (this.lastReport) return this.lastReport;
//         throw new Error('No report Found !!!');
//     }

//     set prevReport(value: string) {
//         if (!value) throw new Error('Please pass in Valid Report');
//         this.addReport(value);
//     }

//     constructor(id: number, private reports: string[]) {
//         super('Human Resource', id);
//         this.lastReport = reports[0];
//     }

//     addEmployee(...name: string[]) {
//         if (name[1] === 'Dimple Tyagi') return;
//         this.employees.push(...name);
//     }

//     addReport(text: string) {
//         this.reports.push(text);
//         this.lastReport = text;
//     }

//     printReports() {
//         console.log(this.reports);
//     }
// }

// const hrdep = new HrDepartment(0o3, []);

// // hrdep.prevReport = ' ';
// // hrdep.addEmployee('Rita Malik', 'Aman Vats', 'Sukriti Kumari');
// // hrdep.printEmployee();
// console.log(hrdep.prevReport);
// hrdep.addReport('MEAN Team is Underperforming...');
// // hrdep.addReport('Python Team is Performing Phenomenonally...');
// // console.log(hrdep);
// // hrdep.printReports();
