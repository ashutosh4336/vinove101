// // Object Literlas

// const s1 = 'Hello';
// console.log(s1.toUpperCase());
// console.log(typeof s1);

// const s2 = new String('Ashutosh');
// console.log(s2);
// console.log(typeof s2);

// console.log(navigator.appVersion);

// const bookOne = {
//   title: 'Art of War',
//   author: 'Ashutosh Panda',
//   year: 2020,
//   getSummary: function () {
//     return `${this.title} was wriiten by ${this.author} in the Year ${this.year}`;
//   },
// };

// // console.log(bookOne.getSummary());
// console.log(Object.values(bookOne));
// console.log(Object.keys(bookOne));

// // construcutor //
// function Book(title, author, year) {
//   this.title = title;
//   this.author = author;
//   this.year = year;

//   //   this.getSummary = function () {
//   //     return `${this.title} was written by ${this.author} in the year ${this.year}`;
//   //   };

//   //   console.log(
//   //     `${this.title} was written by ${this.author} in the year ${this.year}`
//   //   );
// }

// // GetSummary
// Book.prototype.getSummary = function () {
//   return `${this.title} was written by ${this.author} in the year ${this.year}`;
// };

// // // GetAge
// // Book.prototype.getAge = function () {
// //   const years = new Date().getFullYear() - this.year;
// //   return `${this.title} is ${years} Old`;
// // };

// // // Revise - Chage Year
// // Book.prototype.revise = function (newYear) {
// //   this.year = newYear;
// //   this.revised = true;
// // };

// // const bookOne = new Book('Art of War', 'Ashutosh Panda', 2007);
// // const bookTwo = new Book('Kuch V', 'Ashutosh Panda', 2020);

// // // console.log(bookOne);
// // // console.log(bookOne.getSummary());
// // // console.log(bookOne.getAge());
// // console.log(bookOne);

// // bookOne.revise(2010);
// // console.log(bookOne);
// // // console.log(bookOne.getSummary());

// // Magazine Constructor
// function Magazine(title, author, year, month) {
//   Book.call(this, title, author, year);

//   this.month = month;
// }

// // Inherit Property
// Magazine.prototype = Object.create(Book.prototype);

// // Instantiate Magazine Object
// const magOne = new Magazine('Mag one', 'jhon Doe', '2018', 'March');

// console.log(magOne.getSummary());

// // console.log(Object.keys(magOne));
// // console.log(Object.values(magOne));

// Object of Protos
// const bookProtos = {
//   getSummary: function () {
//     return `${this.title} was written by ${this.author} in ${this.year}`;
//   },
//   getAge: function () {
//     const years = new Date().getFullYear() - this.year;
//     return `${this.title} is ${years} Years old`;
//   },
// };

// // Create Object
// // const bookOne = Object.create(bookProtos);
// // bookOne.title = 'Book One';
// // bookOne.author = 'Jhon Doe';
// // bookOne.year = 2014;

// const bookOne = Object.create(bookProtos, {
//   title: { value: 'Art of War' },
//   author: { value: 'Jhon Doe' },
//   year: { value: 2013 },
// });

// console.log(bookOne);

// Classes

// class Book {
//   constructor(title, author, year) {
//     this.title = title;
//     this.author = author;
//     this.year = year;
//   }

//   getSummary() {
//     return `${this.title} was written by ${this.author} in ${this.year}`;
//   }

//   getAge() {
//     const years = new Date().getFullYear() - this.year;
//     return `${this.title} is ${years} Years old`;
//   }

//   revise(newYear) {
//     this.year = newYear;
//     this.revised = true;
//     return `${this.title} is revised in ${this.year} `;
//   }

//   static topbookStore() {
//     return 'Money Heist';
//   }
// }

// const bookOne = new Book('Art of War', 'Ashutosh Panda', 2015);

// console.log(bookOne);
// console.log(bookOne.getSummary());
// console.log(bookOne.getAge());
// console.log(bookOne.revise(2016));
// console.log(bookOne);

// console.log(Book.topbookStore());

// SubClass

class Book {
  constructor(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;
  }

  getSummary() {
    return `${this.title} was written by ${this.author} in ${this.year}`;
  }
}

// Magazine Subclass

class Magazine extends Book {
  constructor(title, author, year, month) {
    super(title, author, year);
    this.month = month;
  }
}

// Instantiate Mangazine
const magOne = new Magazine('Vox', 'Vox Editor', 2020, 'March');
console.log(magOne);
console.log(magOne.getSummary());
