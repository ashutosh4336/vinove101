// Book Class: Represent a Book
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

// UI CLass: Handle UI Task
class UI {
  static displayBooks() {
    // const storeBooks = [
    //   {
    //     title: 'BookOne',
    //     author: 'Jhon Doe',
    //     isbn: '3538'
    //   },
    //   {
    //     title: 'BookTwo',
    //     author: 'Shraddha Kapoor',
    //     isbn: '4336'
    //   },
    //   {
    //     title: 'BookThree',
    //     author: 'Pinky Panda',
    //     isbn: '3748'
    //   }
    // ];
    const books = Store.getBooks();
    // console.log(typeof books);

    books.forEach(book => UI.addBooktoList(book));
  }

  static addBooktoList(book) {
    const list = document.querySelector('#book-list');

    const row = document.createElement('tr');
    row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href="#" class="btn btn-danger btn-sm delete">X</td>
    `;
    list.appendChild(row);
  }

  static deleteBook(el) {
    if (el.classList.contains('delete')) {
      el.parentElement.parentElement.remove();
    }
  }

  static showAlert(message, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.appendChild(document.createTextNode(message));
    const container = document.querySelector('.container');
    const form = document.querySelector('#book-form');

    container.insertBefore(div, form);

    setTimeout(() => document.querySelector('.alert').remove(), 3000);
  }

  // Clear Fields
  static clearFields() {
    document.querySelector('#title').value = '';
    document.querySelector('#author').value = '';
    document.querySelector('#isbn').value = '';
  }
}

// Store class
class Store {
  static getBooks() {
    let books;
    if (localStorage.getItem('books') === null) {
      books = [];
    } else {
      books = JSON.parse(localStorage.getItem('books'));
    }

    return books;
  }

  static addBook(book) {
    const books = Store.gebooks();

    books.push(book);

    localStorage.setItem('books', JSON.stringify(books));
  }

  static removeBook() {
    const books = Store.getbooks();

    books.forEach((book, index) => {
      if (book.isbn === isbn) {
        books.splice(index, 1);
      }
    });

    localStorage.setItem('books', JSON.stringify(books));
  }
}

// Store class:Handle Storage

// Event Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks);
// Event Add a Book

document.querySelector('#book-form').addEventListener('submit', e => {
  e.preventDefault();
  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const isbn = document.querySelector('#isbn').value;

  // Validate
  if (title === '' || author === '' || isbn === '') {
    UI.showAlert('Please Fill all the Fields', 'danger');
  } else {
    // Instatiate Book
    const book = new Book(title, author, isbn);

    // console.log(book);
    UI.addBooktoList(book);

    // Add book to storeage
    Store.addBook(book);

    // Show success Message
    UI.showAlert('Book Added', 'success');

    // Clear Field
    UI.clearFields();
  }
});

// Event to Remove a Book

document.querySelector('#book-list').addEventListener('click', e => {
  UI.deleteBook(e.target);
});
