// console.log('Ashutosh');
const form = document.getElementById('addForm');
const itemList = document.getElementById('items');
const btn = document.getElementById('btn-test');

// ForSubmitEvent

form.addEventListener('submit', addItem);
btn.addEventListener('click', changeBackground);

function addItem(e) {
  e.preventDefault();
  // get Input value
  const newItem = document.getElementById('item').value;
  // Create New li Elements
  const li = document.createElement('li');
  // Add className
  li.className = 'list-group-item';
  // Add TextNode
  const textNode = document.createTextNode(newItem);
  li.appendChild(textNode);

  // New Delete Button //
  const newBtn = document.createElement('button');
  // Add class
  newBtn.className = 'btn btn-danger btn-sm float-right delete';
  // TextNode
  const xBtn = document.createTextNode('X');
  newBtn.appendChild(xBtn);
  li.appendChild(newBtn);
  itemList.appendChild(li);
}

function changeBackground() {
  const title = document.querySelector('#nav-cont');
  title.parentNode.className = 'bg-info text-white p-4 mb-3 display-4';
  title.textContent = 'Hello Amigos';
}
