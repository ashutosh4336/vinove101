import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputPostComponent } from './output-post.component';

describe('OutputPostComponent', () => {
  let component: OutputPostComponent;
  let fixture: ComponentFixture<OutputPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
