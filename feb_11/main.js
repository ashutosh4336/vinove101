// var name = 'Vinove';

// function abc() {
//   var data = 'Value Coders';
//   function s() {
//     console.log(data);
//   }
//   return 0;
// }

// var empCode = abc();
// empCode();

// const [a, , , , , , , b, ...rest] = alphabates;
// console.log(rest);
// // console.log(alphabates[0]);
// // console.log(alphabates[1]);

// // Destructuring
// const [a, , , c, ...rest] = alphabates;
// // const newArr = [...alphabates, ...numbers];

// const newArr = alphabates.concat(numbers);

// console.log(newArr);

// function sumMultiply(a, b) {
//   return [a + b, a * b];
// }
// const [sum, multi] = sumMultiply(4, 6);
// console.log(sum);
// aaa();

const personOne = {
  name: 'Ashutosh',
  age: 21,
  favoriteFood: 'Rice',
  address: {
    apartment: 'Aashiana Aprtments',
    pin: 210309,
    city: 'Noida',
    state: 'UP'
  }
};

// const { name: firstName, age, favoriteFood = 'Khir' } = personOne;
// console.log(firstName, age, favoriteFood);

// let p = new Promise((resolve, reject) => {
//   let a = 5 + 7;
//   if (a === 11) {
//     resolve('Success');
//   } else {
//     reject('Failed');
//   }
// });

// p.then(message => console.log(`this is in Then ${message}`)).catch(message =>
//   console.log(`this in Catch ${message}`)
// );

// const userLeft = true;
// const userWatching = true;

// function watch() {
//   return new Promise((resolve, reject) => {
//     if (!userWatching) {
//       reject({
//         name: 'User Left',
//         message: ':('
//       });
//     } else if (userWatching) {
//       reject({
//         name: 'User watching Money Heist',
//         message: 'PwnDev'
//       });
//     } else {
//       resolve('Succed');
//     }
//   });
// }

// watch()
//   .then(message => console.log(`Success ${message}`))
//   .catch(message => console.log(message.name, message.message));

const companies = [
  { name: 'Company One', category: 'Finance', start: 1981, end: 2004 },
  { name: 'Company Two', category: 'Retail', start: 1992, end: 2008 },
  { name: 'Company Three', category: 'Auto', start: 1986, end: 1996 },
  { name: 'Company Eight', category: 'Technology', start: 2011, end: 2016 },
  { name: 'Company Nine', category: 'Retail', start: 1981, end: 1989 }
];

const alphabates = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

// let arrayy = [];
// const filterNum = numbers.filter(num => {
//   if (num < 13) {
//   console.log()
//   }
// });

function isLandscape(width, height) {
  // if (width > height) {
  //   console.log(`Image is Landscape`);
  // } else {
  //   console.log(`Image is Not LandScape`);
  // }
  return width > height;
}

// console.log(isLandscape(60, 12));

// FizzBuzz
// function fizzBuzz(input) {
//   if (input % 3 === 0 && input % 5 === 0) {
//     console.log(`FizzBuzz`);
//   } else if (input % 5 === 0) {
//     console.log(`Buzz`);
//   } else if (input % 3 === 0) {
//     console.log('Fizz');
//   } else {
//     console.log('input is invalid');
//   }
// }

// fizzBuzz(45);

function checkSpeeLimit(input) {
  const speedLimit = 70;
  const kmPerPoint = 5;
  if (input < speedLimit + kmPerPoint) {
    console.log('ok');
  } else {
    let points = Math.floor((input - speedLimit) / kmPerPoint);
    if (points >= 12) {
      console.log(`License Suspended`);
    } else {
      console.log(`Points: ${points}`);
    }
  }
}

// checkSpeeLimit(84);

// function oddEve(limit) {
//   for (let i = 0; i <= limit; i++) {
//     //   if (i % 2 === 0) {
//     //     console.log(`${i}  is Even`);
//     //   } else {
//     //     console.log(`${i}  is Odd`);
//     //   }

//     const message = i % 2 === 0 ? 'Even' : 'Odd';
//     console.log(`${i} id ${message}`);
//   }
// }
// oddEve(11);

// Array.isArray(arrayName);
// return numbers instanceof Array;

const isActive = true;
const firstName = 'Ashutosh'; // Truthy
const lastName = ''; //  Falsy ==> undefined, null, '', false, 0, NaN
// if (lastName) console.log(`Hello`);

const array1 = ['', 0, 1, 2, 3, 4, 5, 6, 7, 8, NaN, undefined, null, false];

// function countTruthy(array) {
//   let count = 0;
//   for (let value of array) if (value) count++;
//   console.log(count);
// }
// countTruthy(array1);

// Show Property
const movie = {
  name: 'Ready or Not',
  releaseYear: 2020,
  rating: 8.9,
  gener: 'Thriller'
};

// function showProps(input) {
//   // for (let key in input) console.log(key);
//   for (let key in input) console.log(`${key}: ${input[key]}`);
// }
// showProps(movie);

// Sum of Numbers that is Divisible by Either 3 or 5 in a given Limit
// function sum(limit) {
//   let mulThree = [];
//   for (let i = 0; i <= limit; i++)
//     if (i % 3 === 0 || i % 5 === 0) mulThree.push(i);

//   const sumofNum = mulThree.reduce((a, b) => a + b);
//   console.log(sumofNum);
// }

// sum(25);

// Calculate Grade
const numbers = new Array(
  33,
  12,
  20,
  16,
  5,
  54,
  21,
  44,
  61,
  13,
  16,
  47,
  25,
  615,
  500
);

function calGrade(input) {
  const avgGrade = input.reduce((a, b) => a + b) / input.length;
  if (avgGrade <= 59) console.log('F');
  else if (avgGrade === 60 || avgGrade <= 69) console.log('D');
  else if (avgGrade === 70 || avgGrade <= 79) console.log('A');
  else if (avgGrade === 80 || avgGrade <= 89) console.log('E');
  else if (avgGrade === 90 || avgGrade <= 100) console.log('O');
  else console.log(`Input is Invalid`);
  console.log(avgGrade);
}

// calGrade(numbers);

// Show Star
function showStar(rows) {
  for (let row = 1; row <= rows; row++) {
    let pattern = '';
    for (let i = 0; i < row; i++) {
      pattern += '*';
    }
    console.log(pattern);
  }
}

// showStar(10);

// Print Prime Numbers within the range given below

let isPrime = true;
function showPrime(input) {
  for (let i = 2; i <= input; i++) {
    for (let j = 2; j < i; j++) {
      if (i % j === 0) {
        isPrime = false;
        break;
      }
    }
  }
  if (isPrime) console.log(i);
}
showPrime(20);
