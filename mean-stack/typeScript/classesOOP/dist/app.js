"use strict";
class Person {
    constructor(a) {
        this.age = 21;
        this.name = a;
    }
    greet(a) {
        console.log(a + this.name + ", You're " + this.age + ' Years Old');
    }
    education(a) {
        console.log(this.name + ' is persuing ' + a);
    }
}
const userOne = new Person('Ashutosh');
console.log('userOne : ', userOne);
userOne.greet('Hi ');
userOne.education('BTech');
