const posts = [
  { title: 'Post One', body: 'This is Post One' },
  { title: 'Post Two', body: 'This is Post Two' },
  { title: 'Post Three', body: 'This is Post Three' }
];

function getPost() {
  setTimeout(() => {
    let output = '';

    posts.forEach(post => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
}

function createPost(post, callBack) {
  setTimeout(() => {
    posts.push(post);
    callBack();
  }, 2000);
}

createPost({ title: 'Post Four', body: 'This is Post Four' }, getPost);
