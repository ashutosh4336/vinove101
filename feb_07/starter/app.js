// let name = 'Ashutosh';
// let interestRate = 0.3;
// interestRate = 5;

// let person = {
//   name: 'Shruti Pramalik',
//   gender: 'female',
//   age: 20
// };

// let selecteColors = ['red', 'green', 'white', 'black', 'blue'];

// // Dot Notation
// // console.log(selecteColors.indexOf('white'));

// // Bracket Notation
// // console.log(person['age']);

// let x = 10;
// let y = 12;
// // console.log(x + y);
// // console.log(++x);

// // Strict Equality Operator
// // console.log(1 === 1);
// // console.log('1' === 1);

// // Lose Equality Operator
// // console.log(1 == 1);
// // console.log('1' == 1);
// // console.log(false == 0);

// // Ternary Operator
// let points = 110;
// let type = points > 100 ? 'Gold' : 'Silver';
// // console.log(type);

// let highIncome = true;
// let goodCreditScore = true;

// // Logical OR
// let eligiblleForLoan = highIncome && goodCreditScore;

// // NOT (!)
// let applicationRefused = !eligiblleForLoan;
// // console.log(applicationRefused);

// let userColor = 'red';
// let defaultColor = 'blue';
// let currentColor = userColor || defaultColor;
// // console.log(currentColor);

// // Bitwise Operator

// // 1 = 00000001
// // 2 = 00000010
// // 3 = 00000011
// // 4 = 00000100
// // 5 = 00000101
// // 6 = 00000110
// // 7 = 00000111
// // 8 = 00001000
// // 9 = 00001001
// // 10 = 00001010
// // 11 = 00001011
// // 12 = 00001100
// // 13 = 00001101
// // 14 = 00001110
// // 15 = 00001111

// // console.log(1 | 2); // Bitwise OR
// // console.log(2 | 14); // Bitwise OR

// // console.log(1 & 2); // Bitwise AND

// // Read, Write, Execute
// // 00000100
// // 00000010
// // 00000001

// const readPermission = 4;
// const writePermission = 2;
// const executePermission = 1;

// let myPermission = 0;
// myPermission = myPermission | readPermission | writePermission;
// let message = myPermission & writePermission ? 'yes' : 'no';
// // console.log(message);

// let a = 10;
// let b = 20;
// let c = a;
// a = b;
// b = c;

// console.log(a);
// console.log(b);

// let hour = 9;
// if (hour >= 6 && hour < 12) console.log(`Good Morning, It's ${hour}`);
// else if (hour >= 12 && hour < 18) console.log(`Good Afternoon`);
// else console.log(`Good evening`);

let role;
role = 'guest';
// switch (role) {
//   case 'guest':
//     console.log('Guest user');
//     break;
//   case 'moderator':
//     console.log('Moderator User');
//     break;

//   default:
//     console.log('Unknown Role');
// }

// if (role === 'guest') console.log('Guest user');
// else if (role === 'moderator') console.log('Moderator User');
// else console.log('Unoknown User');

// let name = {
//   fitrstName: 'Ashutosh',
//   place: {
//     birth: 'Mukhigura',
//     school: 'balangir',
//     college: 'bhubaneswar',
//     job: 'Noida'
//   },
//   age: 21
// };

// For Loop
// for (let i = 0; i <= 5; i++) {
//   if (i % 2 === 0) console.log(i);
// }

// While Loop
// let i = 0;
// while (i <= 10) {
//   if (i % 2 !== 0) console.log(i);
//   i++;
// }

// Do While
// let i = 3;
// do {
//   if (i % 2 !== 0) console.log(i);
//   i++;
// } while (i <= 7);

// let i = 0;

// while (i <= 5) {
//   console.log(i);
//   i++;
// }

// Clouser
// function myFunc() {
//   let name = 'Ashutosh';
//   function displayName() {
//     console.log(name);
//   }

//   return displayName;
// }

// const myName = myFunc();
// myName();

// Clouser
// function myFunc() {
//   let name = 'Ashutosh Panda';
//   function displayName() {
//     console.log(name);
//   }
//   return displayName;
// }

// const myName = myFunc();
// myName();

// for-in loop
// const person = {
//   name: 'Ashutosh',
//   occupation: 'junior associate software developer',
//   age: 21
// };

// for (let i in person) console.log(i, person[i]);

// const color = ['red', 'blue', 'green'];
// for (let index in color) console.log(index, color[index]);

// ForOF loop
// const colors = ['white', 'blue', 'green'];
// for (let color of colors) console.log(color);

// forEach
// colors.forEach(kuchV => console.log(kuchV));

let i = 0;
while (i <= 10) {
  // if (i === 9) break;
  if (i % 2 === 0) {
    i++;
    continue;
  }

  console.log(i);
  i++;
}
