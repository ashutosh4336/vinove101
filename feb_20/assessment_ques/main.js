// Question 1
// const form = document.createElement('form');
// const input1 = document.createElement('input');
// const input2 = document.createElement('input');
// const btn = document.createElement('button');
// const outputArea = document.createElement('p');

// const body = document.querySelector('body');

// btn.id = 'btn';
// btn.setAttribute('type', 'submit');
// btn.textContent = 'Submit';
// input1.placeholder = 'Enter First Name';
// input2.placeholder = 'Enter Last Name';

// input1.style.display = 'block';
// input2.style.display = 'block';
// btn.style.marginTop = '1rem';
// btn.style.backgroundColor = '#3e3e3e';
// btn.style.color = '#fff';
// btn.style.padding = '1rem';
// btn.style.borderRadius = '5px';

// form.appendChild(input1);
// form.appendChild(input2);
// form.appendChild(btn);
// body.appendChild(form);

// form.addEventListener('submit', e => {
//   e.preventDefault();

//   const firstInputvalue = input1.value;
//   const secondInputvalue = input2.value;

//   outputArea.innerHTML = `${firstInputvalue} ${secondInputvalue}`;

//   body.appendChild(outputArea);
// });

// -----------------------------------------------------------------------------------//

// // Question 2

// arr = [1, 2, 4, 5, 6, 5, 58, 48, 5, 4, 15, 4, 84, 874, 89];
// let temp;
// let empArr = [];
// function showSecondLargest(arr) {
//   for (let i = 0; i < arr.length; i++) {
//     for (let j = i + 1; j < arr.length; j++) {
//       if (arr[i] > arr[j]) {
//         temp = arr[i];
//         arr[i] = arr[j];
//         arr[j] = temp;
//       }
//     }
//     empArr.push(arr[i]);
//   }
//   console.log(empArr[1]);
// }
// showSecondLargest(arr);

// ------------------------------------------------------------------------//

// Question 3 Frequency of an Alphabate
// const sent = 'Hi People I love JavaScript !!!';
// const charMap = {};
// const newSent = sent
//   .replace(/[^A-Z]/gi, '')
//   .toLowerCase()
//   .split('');
// // console.log(newSent);
// newSent.forEach(char => {
//   charMap[char] ? charMap[char]++ : (charMap[char] = 1);
// });
// console.log(charMap);

// ------------------------------------------------------------------------//

// MCQs
// // // // 1 \\ \\ \\ \\
// exp();
// var exp = function() {
//   console.log('Hi');
// };

// // // // 2 \\ \\ \\ \\
// console.log(null || undefined || []);

// // // // 3 \\ \\ \\ \\
// const str = 'javascript';
// console.log(str.slice(4, 6));

// // // // 4 \\ \\ \\ \\
// for (var i = 0; i <= 5; i++) {}
// console.log(i);

// // // // 5 \\ \\ \\ \\
// var v = 1;
// function foo() {
//   console.log(v);
// }
// v = 2
// foo()

// // // // 6 \\ \\ \\ \\
// console.log(typeof [4, 5]);

// // // // 7 \\ \\ \\ \\
// Define Clouser and Write a Piece of Code to Explain
