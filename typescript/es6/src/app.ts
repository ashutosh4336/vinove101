export {};
// const name = 'ashutosh';
// console.log(name);
// https://kangax.github.io/compat-table/es6/

const add = (...num: number[]) => {
  return num.reduce((a, b) => {
    return a + b;
  }, 0);
};

const resultAdd = add(4, 56, 8, 9, 6, 5, 6, 6);
console.log(resultAdd);

const printResult: (a: number | string) => void = (output) =>
  console.log(output);

const person = {
  name: 'Ashutosh Panda',
  age: 21,
};

const copiedPerson = { ...person, address: 'Noida Delhi India' };
// console.log(copiedPerson);

const hobbies = ['sports', 'computers'];
const newHobbies = ['Netflix', 'cooking'];
const finalHobbies = [...hobbies, ...newHobbies];
console.log(finalHobbies);

const [firstHobby, secondHobby] = finalHobbies;

console.log(firstHobby);
console.log(secondHobby);

const { name, age, address } = copiedPerson;
// console.log(name, age, address);
