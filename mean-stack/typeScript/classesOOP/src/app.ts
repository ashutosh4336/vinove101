// type Greetperson = {
//   name: string;
//   age: number;
//   greet(input: string): void;
// };

// let userOne: Greetperson;
// userOne = {
//   name: 'Ashutosh Panda',
//   age: 21,
//   greet(a) {
//     console.log(a + this.name);
//   },
// };

// userOne.greet('Hi ');

// Interface & Implements

interface NameConv {
  readonly name: string;
  age: number;
  fatherName?: string /* Option '?' */;
}
interface Greetperson extends NameConv {
  // readonly name: string;
  // name: string;
  // age: number;
  greet(input: string): void;
}

class Person implements Greetperson {
  name: string;
  age = 21;

  constructor(a: string) {
    this.name = a;
  }

  greet(a: string) {
    console.log(a + this.name + ", You're " + this.age + ' Years Old');
  }

  education(a: string) {
    console.log(this.name + ' is persuing ' + a);
  }
}
const userOne = new Person('Ashutosh');
console.log('userOne : ', userOne);

userOne.greet('Hi ');
userOne.education('BTech');
/*  =====================================================  */

// Custom Type Function Declration
// type Addfn = (n1: number, n2: number) => number;

// Interface
// interface Addfn {
//   (n1: number, n2: number): number;
// }

// let addNumber: Addfn = (a: number, b: number) => a + b;
// console.log(addNumber(74, 6));
