import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GithubService } from 'src/app/services/github.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  user = null;
  userName: string;
  Error = null;

  constructor(
    private githubService: GithubService,
    private ref: ChangeDetectorRef,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  handleFindUser() {
    this.githubService.getUserDeatils(this.userName).subscribe(
      (el) => {
        this.user = el;
        this.Error = null;

        this.ref.detectChanges();
      },
      (err) => {
        this.user = null;
        this.Error = 'User Not found';
        this.ref.detectChanges();
        this.toastr.error(`${err} User not Found`);
      }
    );
  }
}
