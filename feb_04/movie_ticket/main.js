const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.occupied)');

const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');

// const ticketPrice = parseInt(movieSelect.value);
let ticketPrice = +movieSelect.value;

// console.log(ticketPrice);

// Function updateSelectedCount
function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  // copy Selected seats into an array
  // Map throough that array
  // return a new array with value
  const seatsIndex = [...selectedSeats].map(seat => [...seats].indexOf(seat));
  // console.log(seatsIndex);

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  const selectedSeatsCount = selectedSeats.length;
  //   console.log(selectedSeatsCount);
  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice;
}

// Movie select Event
movieSelect.addEventListener('change', e => {
  ticketPrice = parseInt(e.target.value);
  console.log(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

// Seat click Event
container.addEventListener('click', e => {
  if (
    e.target.classList.contains('seat') &&
    !e.target.classList.contains('occupied')
  ) {
    e.target.classList.toggle('selected');

    updateSelectedCount();
  }
});
