import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SuccessAlertComponent } from './Components/success-alert/success-alert.component';
import { WarningAlertComponent } from './Components/warning-alert/warning-alert.component';
import { ServerComponent } from './Components/server/server.component';

@NgModule({
  declarations: [AppComponent, SuccessAlertComponent, WarningAlertComponent, ServerComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
