import { Component, OnInit } from "@angular/core";
import { PostService } from "src/app/services/post.service";
import { PostStruct } from "src/app/model/Post";

@Component({
  selector: "app-output-post",
  templateUrl: "./output-post.component.html",
  styleUrls: ["./output-post.component.css"],
})
export class OutputPostComponent implements OnInit {
  posts: any[] = [];

  constructor(private postServ: PostService) {}

  ngOnInit(): void {
    this.postServ.getPosts().subscribe(
      (post) => {
        // console.log(post);
        this.posts = post.data;
      },

      (err) => {
        console.error(err);
      }
    );
  }
}
