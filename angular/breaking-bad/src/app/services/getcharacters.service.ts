import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GetcharactersService {
  url: string = 'https://www.breakingbadapi.com/api/characters';

  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get(`${this.url}`);
  }
}
