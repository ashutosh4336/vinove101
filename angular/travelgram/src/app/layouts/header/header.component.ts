import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  email = null;
  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    auth.getUser().subscribe((user) => {
      // console.log('User is: ' + user);
      this.email = user?.email;
    });
  }

  ngOnInit(): void {}

  async handleSignOut() {
    try {
      await this.auth.signOut();
      this.toastr.info('Login to Continue again', '', {
        closeButton: true,
      });
      this.router.navigateByUrl('/signin');
      this.email = null;
    } catch (err) {
      this.toastr.error("Something Went Wrong. Couldn't Signout", '', {
        closeButton: true,
      });
    }
  }
}
