const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const colors = require('colors');
const morgan = require('morgan');

dotenv.config({ path: './config/config.env' });

const transactions = require('./routes/transactions');
const connectDB = require('./config/db');
const app = express();
app.use(express.json());
connectDB();
app.use('/api/v1/transactions', transactions);

const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(
    `Server strated in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);
