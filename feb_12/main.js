// // {
// //   const name = 'Ashutosh';
// // }
// // console.log(name);

// function playVideo() {
//   console.log(this);
// }

// // playVideo.call({ name: 'Ashutosh' });
// // playVideo.apply({ name: 'Ashutosh' }, [1, 2, 3]);
// // playVideo.bind({ name: 'Ashutosh' });
// // input = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 6, 47, 25, 615, 500];

// function sum(...items) {
//   //   console.log(items[0]);

//   if (items.length === 1 && Array.isArray(items[0])) items = [...items[0]];
//   const result = items.reduce((a, b) => a + b);
//   console.log(result);
// }

// // sum(input);

// // console.log(input.sort((a, b) => a - b));

// // Circle Radius
// // circle.radius = 2
// // circle.area = ?

// // function areaCircle(radius) {
// //   return Math.PI * Math.pow(radius, 2);
// // }
// // console.log(areaCircle(5));

// //-------------------------------------------------------//
// // ReadOnly Area
// // const circle = {
// //   radius: 5,
// //   get area() {
// //     return Math.PI * Math.pow(this.radius, 2);
// //   }
// // };
// // console.log(circle.area);

// // Count Occurannce

// // function countOcc(arr, num) {
// //   if (!Array.isArray(arr)) throw new Error('Invalid Input');
// //   return arr.reduce((acc, current) => {
// //     const occuRance = current === num ? 1 : 0;
// //     return acc + occuRance;
// //   });
// // }

// // try {
// // input = [33, 12, 20, 16, 33, 54, 33, 44, 16, 33, 16, 47, 33, 615];
// //   const count = countOcc(input, 16);
// //   console.log(count);
// // } catch (err) {
// //   console.log(err.message);
// // }

// // function sum(input) {
// //   // console.log(arguments);
// //   let total = 0;
// //   for (let value of arguments) {
// //     total += value;
// //   }
// //   return total;
// // }

// // console.log(sum(1, 2, 3, 4, 5, 6, 7, 8, 9));

// // function sum(...args) {
// //   let total = 0;
// //   for (let value of arguments) {
// //     total += value;
// //   }
// //   console.log(total);
// // }

// // sum(1, 4, 5, 6, 8);

// // function interst(principal, years, rate = 12.5) {
// //   // rate = rate || 12.5;
// //   // years = years || 10;
// //   return ((principal * rate) / 100) * years;
// // }

// // console.log(interst(4500, 10));

// const person = {
//   firstName: 'Ashutosh',
//   lastName: 'Panda',
//   age: 21,
//   // get fullName() {
//   //   return `${this.firstName} ${this.lastName}`;
//   // },
//   set fullName(value) {
//     if (typeof value !== 'string') {
//       throw new Error("Value isn't a String");
//     }

//     const parts = value.split(' ');
//     if (parts.length !== 2) {
//       throw new Error('Enter a first and last Name');
//     }
//     this.firstName = parts[0];
//     this.lastName = parts[1];
//   }
// };

// try {
//   person.fullName = 'Arvind ';
//   person.age = 20;
// } catch (err) {
//   console.error(err.message);
// }

// getters = access property
// setters = change (mutate) them

// console.log(`${person.firstName} ${person.lastName} with age ${person.age}`);
// console.log(person);

// input = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 6, 47, 25, 615, 500];
// const result = input.reduce(function aa(a, b) {
//   return a + b;
// });
// console.log(result);

// function formatdate(userDate) {
//   const res = userDate.replace(/[^\w\s]/gi, ' ');
//   return res
//     .split(' ')
//     .reverse()
//     .join('');
//}
// formatdate('31/03/1999');
// console.log(formatdate('31/03/1999'));

// const isPrime = function(number) {
//   const divisor = parseInt(number / 2, 10);
//   const prime = true;
//   while (divisor > 1) {
//     if (number % divisor === 0) {
//       prime = false;
//       divisor = 0;
//     } else {
//       divisor -= 1;
//     }
//   }
//   return prime === true;
// };
// function negate(f) {
//   return function(i) {
//     return !f(i);
//   };
// }
// const isComposite = negate(isPrime); // function object
// alert([2, 4].every(isComposite)); // => false (2 is prime, 4 is not)
// alert([4, 6].every(isComposite)); // => true (4 or 6 are composite)

// Factory function

// function createCircle(radius) {
//   return {
//     radius,
//     draw() {
//       console.log(`Draw`);
//     }
//   };
// }
// const circle1 = createCircle(2);
// // console.log(circle1);

// function ab() {
//   console.log(this);
// }
// ab();

// Constructor Function
// function Circle(radius) {
// console.log(this);
// this.radius = radius;
// this.draw = function() {
// console.log('Draw');
// };
// }

// Circle(5);

// const circle = new Circle(5);
// console.log(circle);

// const circle = {
//   radius: 5
// };
// circle.color = 'red';
// circle.draw = function() {};
// delete circle.color;
// delete circle.draw;
// console.log(circle);

// function Circle(radius) {
//   this.radius = radius;
//   this.draw = function() {
//     console.log('Draw');
//   };
// }

// console.log(Circle.call({}, 5));
// Circle.apply({}, [1, 5, 6]);

// const circle = new Circle(5);
// console.log(circle);

// x.value = 30;
// console.log(x);

// Primitives are Copied by Values
// Objects are Copied by Reference

// let number = 10;
// function increase(number) {
//   number++;
//   console.log(number);
// }
// console.log(increase(number));
// console.log(number);

// const x = { value: 20 };
// function increase(obj) {
//   obj.value++;
// }

// increase(x);
// console.log(x);

const circle = {
  radius: 10,
  draw() {
    console.log('Draw');
  }
};

// for (let key in circle) {
//   console.log(key, circle[key]);
// }

// For of loop can only be used with Iterables
// such as arrays maps
// an Object isn't a iterable
// for (let key of circle) console.log(key);
for (let key of Object.keys(circle)) console.log(key);
