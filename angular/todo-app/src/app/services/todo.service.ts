import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Todo } from '../model/Todo';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  todos: Todo[];
  constructor() {
    this.todos = [
      {
        id: '001',
        title: 'Learn Angular',
        isCompleted: false,
        date: new Date(),
      },
      {
        id: '002',
        title: 'Learn React',
        isCompleted: false,
        date: new Date(),
      },
      {
        id: '003',
        title: 'Learn Vue',
        isCompleted: false,
        date: new Date(),
      },
      {
        id: '004',
        title: 'Learn Python & Django',
        isCompleted: true,
        date: new Date(),
      },
    ];
  }

  getTodos() {
    return of(this.todos);
  }

  addTodo(todo: Todo) {
    this.todos.push(todo);
  }

  changeStatus(todo: Todo) {
    this.todos.map((el) => {
      if (el.id === todo.id) {
        todo.isCompleted = !todo.isCompleted;
      }
    });
  }

  deleteATodo(todo: Todo) {
    const indexofTodo = this.todos.findIndex((cu) => cu.id === todo.id);
    this.todos.splice(indexofTodo, 1);
  }
}
