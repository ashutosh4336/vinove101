// console.log('Your Code Goes Here !!!');

function add(n1: number, n2: number, showResult: boolean) {
  if (showResult) {
    console.log(n1 + n2);
  } else {
    return n1 + n2;
  }
}

const numberOne = '56';
const numberTwo = 6.78;
const printResult = true;

let resultPhrase: string = 'The answer is :  ';
// let resultPhraseTwo: number = 'The answer is :  ';
// resultPhrase = 0; --> Wrong

add(+numberOne, +numberTwo, printResult);
// console.log(res);
