class Node {
  constructor(data, next = null) {
    this.data = data;
    this.next = next;
  }
}

const n1 = new Node(100);
// console.log(n1);

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  // Insert First Node

  insertFirst(data) {
    this.head = new Node(data, this.head);
    this.size++;
  }

  // Insert the Last Node

  insertLast(data) {
    let node = new Node(data);
    let current;
    //   if Empty make it head
    if (this.head == null) {
      this.head = node;
    } else {
      current = this.head;
      while (current.next) {
        current = current.next;
      }

      current.next = node;
    }

    this.size++;
  }

  // Insert at Index

  insertAt(data, index) {
    //   If Index is out of range
    if (index > 0 && index > this.size) {
      return;
    }

    //   If first Index
    if (index === 0) {
      this.head = new Node(data, this.head);
      return;
    }

    const node = new Node(data);
    let current, previous;

    // Set Current to first
    current = this.head;
    let count = 0;

    while (count < index) {
      previous = current; //Node Before Index
      count++;
      current = current.next;
    }

    node.next = current;
    previous.next = node;

    this.size++;
  }

  // Get at Index

  // Remove at index

  //  Clear Index

  // Print list Data

  printListdata() {
    let current = this.head;

    while (current) {
      console.log(current.data);
      current = current.next;
    }
  }
}

const ll = new LinkedList();
// ll.insertFirst(300);
// ll.insertFirst(200);
ll.insertFirst(100);
// console.log(ll);
ll.insertLast(400);

ll.insertAt(500, 1);
ll.printListdata();
