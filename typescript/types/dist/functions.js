"use strict";
function add(n1, n2) {
    return n1 + n2;
}
function printResult(num) {
    console.log(`The Result is : ${num}`);
}
printResult(add(9, 45));
let combineValues;
combineValues = add;
console.log('Combined Value is', combineValues(5, 8));
function addHandle(n1, n2, cb) {
    const result = n1 + n2;
    cb(result);
}
addHandle(10, 20, (result) => console.log(result));
