const program = require('commander');
const check = require('../commands/check');

program
  .command('price')
  .description('Check Price of Coin')
  .option(
    '--coin <type>',
    'Add Specific Coin Type in CSV format',
    'BTC,ETH,XRP'
  )
  .option('--cur <currency>', 'Change the Currency', 'USD')
  .action((cmd) => check.price(cmd));

program.parse(process.argv);
