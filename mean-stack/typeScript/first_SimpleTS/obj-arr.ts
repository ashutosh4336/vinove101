// const person: {
//   name: string;
//   age: number;
// } = {
// const person: {
//   name: string;
//   age: number;
//   hobbies: string[];
//   marks: number[];
//   role: [number, string]; // Tuples in typescript
// } = {
//   name: 'Ashutosh',
//   age: 21,
//   hobbies: ['sports', 'computers', 'cooking'],
//   marks: [8, 5, 6, 7, 3, 9],
//   role: [5424, 'Junior Developer'],
// };

// person.role.push('admin');       [Right, push is Exception]
// person.role[1] = 56;             [Wrong]
// console.log(person.role);

// for (const hobby of person.hobbies) console.log(hobby.toUpperCase());
// let empArr = [];
// for (const mark of person.marks) {
//   empArr.push(mark);
// }
// empArr.map((i) => console.log(i * 2));

// Any type accepts any data type

// Enum Type

enum Role {
  ADMIN,
  READ_ONLY,
  AUTHOR,
}

const person = {
  name: 'Ashutosh',
  age: 21,
  hobbies: ['sports', 'computers', 'cooking'],
  marks: [8, 5, 6, 7, 3, 9],
  role: Role.ADMIN,
};

if (person.role === Role.AUTHOR) {
  console.log('User is Author');
} else if (person.role === Role.ADMIN) {
  console.log('User is Admin');
} else if (person.role === Role.READ_ONLY) {
  console.log('User is Read Only User');
} else {
  console.error('Unknown User Type');
}
