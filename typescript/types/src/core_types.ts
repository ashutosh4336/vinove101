/*

CORE TYPES IN  TYPESCRIPT
number ==> 1,2,100, 99.99, -87
string ==> 'Hi', "Hi", `Hi`
boolean ==> true, false
object ==> {id: 1, username: 'ashutosh'}
Array ==> [1,2,3] (can be flexiable and strict)
tuple ==> [1, 'Admin'] Fixed length array
enum ==> enum {SOMETHING, SOMEOTHERTHING}
any ==> *

*/

type usertype = {
  name: string;
  skill: (string | number)[]; //Number
  age: number;
  userId: number;
  qualification: {
    matriculation: number;
    plustwo: number;
  };
  role: [number, string]; // Exactly two number first should be number and second should be string
  //   role is a tuple type
  position: Designation;
};

interface something extends usertype {
  fatherName: string;
}

// enum values can be lowercase
// we can directly assign number values to enum values
// like MANAGER = 4 than EMPLOYEE would be 5 and TEAMLEADER 6 and so on
// we can assign any value of enum to any number or string value

enum Designation {
  MANAGER = 9,
  EMPLOYEE = 100,
  TEAMLEADER = 8,
  INTERN = 'INTERN',
}

const myBio: something = {
  name: 'Ashutosh Panda',
  skill: ['javascript', 'typescript', 'python'],
  age: 21,
  userId: 1,
  qualification: {
    matriculation: 83.83,
    plustwo: 54.67,
  },
  role: [0o1, 'Operation MEAN'],
  position: Designation.INTERN,
  fatherName: 'Rajendra Kumar Panda',
};

// enum Designation {
//   MANAGER,
//   EMPLOYEE,
//   TEAMLEADER,
//   INTERN,
// }

const user: usertype = {
  name: 'Ashutosh Panda',
  skill: ['javascript', 'typescript', 'python'],
  age: 21,
  userId: 1,
  qualification: {
    matriculation: 83.83,
    plustwo: 54.67,
  },
  role: [0o1, 'Operation MEAN'],
  position: Designation.INTERN,
};

// user.role.push('admin');
// user.role.unshift(5);
// user.role.push(5) // Won't work

console.log(user.position);

// for (const el of user.role) console.log(el);
