"use strict";
class Department {
    constructor(name, id) {
        this.name = name;
        this.id = id;
        this.employees = [];
    }
    static createEmployee(name) {
        return { name: name };
    }
    addEmployee(empName) {
        this.employees.push(empName);
    }
    printEmployee() {
        console.log(this.employees);
    }
}
Department.fiscalYear = 2020;
class Itdeaprtment extends Department {
    constructor(id, admins) {
        super('It Department', id);
        this.admins = admins;
        this.admins = admins;
    }
    describe() {
        console.log('It Departments Admins are : ' + this.admins);
    }
}
const itDep = new Itdeaprtment(0o1, ['Dimple Tyagi', 'Ajya Tandon']);
itDep.addEmployee('Parvesh Agrawal');
itDep.describe();
class AccountingDep extends Department {
    constructor(id, reports) {
        super('Accounting Department', id);
        this.reports = reports;
        this.lastReport = reports[0];
    }
    get recentReport() {
        if (this.lastReport)
            return this.lastReport;
        throw new Error('No Reports Found');
    }
    set recentReport(value) {
        if (!value)
            throw new Error('Please Provide a Valid Report');
        this.addReports(value);
    }
    static getInstance() {
        if (this.instance)
            return this.instance;
        return (this.instance = new AccountingDep(0o02, []));
    }
    describe() {
        console.log('Accounting Department - ID : ' + this.id);
    }
    addReports(text) {
        this.reports.push(text);
        this.lastReport = text;
    }
    getReport() {
        console.log(this.reports);
    }
    addEmployee(name) {
        if (name === 'Dimple Tyagi')
            return;
        this.employees.push(name);
    }
    printEmployee() {
        console.log(this.employees);
    }
}
const accDep = AccountingDep.getInstance();
accDep.addEmployee('Ajya Pattanik');
accDep.addEmployee('Dimple Tyagi');
accDep.addReports('The Company has Performed Well Inspite of the Covid-19 Pandemic');
accDep.recentReport = 'Q4 Results are Dispointing';
accDep.addReports('However the total Turnover Decreased by 2%');
accDep.getReport();
accDep.describe();
console.log(accDep);
