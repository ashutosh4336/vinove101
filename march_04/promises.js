const posts = [
  { title: 'Post One', body: 'This is Post One' },
  { title: 'Post Two', body: 'This is Post Two' },
  { title: 'Post Three', body: 'This is Post Three' }
];

function getPost() {
  setTimeout(() => {
    let output = '';

    posts.forEach(post => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
}

function createPost(post) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      posts.push(post);

      const error = false;

      if (!error) {
        resolve();
      } else {
        // console.log(reject);
        reject('Error: Something Went Wrong');
      }
    }, 2000);
  });
}

// createPost({ title: 'Post Four', body: 'This is Post Four' })
//   .then(getPost)
//   .catch(err => console.log(err));

// Promise.all

// const promiseOne = Promise.resolve('Hello World');
// const promiseTwo = 10;
// const promiseThree = new Promise((resolve, reject) => {
//   setTimeout(resolve, 2000, 'GoodBye');
// });

// const promiseFour = fetch('https://jsonplaceholder.typicode.com/users')
//   .then(res => res.json())
//   .catch(err => console.log(err.message, 'Something Went Wrong'));

// const promiseFive = fetch('https://reqres.in/api/products/3').then(res =>
//   res.json()
// );

// Promise.all
// Promise.all([promiseOne, promiseTwo, promiseThree, promiseFour]).then(values =>
//   console.log(values)
// );

// async/await

// async function init() {
//   await createPost({ title: 'Post Five', body: 'This is Post Body' });
//   getPost();
// }

// init();

// Async await with Ftech
//   const res = await fetch('https://jsonplaceholder.typicode.com/users');
async function afetchUser() {
  const res = await fetch('http://dummy.restapiexample.com/api/v1/employee');
  const dataJson = await res.json();
  console.log(dataJson);
  console.log(res);
}

afetchUser();
