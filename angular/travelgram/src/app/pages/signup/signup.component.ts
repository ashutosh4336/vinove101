import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// Form
import { NgForm } from '@angular/forms';
// Rxjs
import { finalize } from 'rxjs/operators';
// Firebase
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';
// image compressor
import { readAndCompressImage } from 'browser-image-resizer';
import { imageConfig } from 'utils/config';
import { v4 as uuidV4 } from 'uuid';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  picture: string =
    'https://learnyst.s3.amazonaws.com/assets/schools/2410/resources/images/logo_lco_i3oab.png';

  uploadPercent: number = null;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage
  ) {}

  ngOnInit(): void {}

  onSubmit(f: NgForm) {
    const { name, email, password, username, country, bio } = f.form.value;

    // validation. sanitization

    this.auth
      .signUp(email, password)
      .then((res) => {
        console.log('RES: ', res);
        const { uid } = res.user;

        this.db.object(`/users/${uid}`).set({
          id: uid,
          name: name,
          email: email,
          instaUserName: username,
          country: country,
          bio: bio,
          picture: this.picture,
        });
      })

      .then(() => {
        this.router.navigateByUrl('/signin');
        this.toastr.success('Account Created');
      })
      .catch((err) => {
        console.log('Error of SignUp' + err);
        this.toastr.error(`${err.message} Signup failed`);
      });
  }

  async uploadFile(event) {
    const file = event.target.files[0];
    let resizedImage = await readAndCompressImage(file, imageConfig);
    // let fileExt = file.name.split('.')
    // fileExt = fileExt.slice(-1)[0]
    const filePath = uuidV4();
    // const fileName = `${filePath}${fileExt}`

    // const fileRef = this.storage.ref(`/profileImages/${filePath}`)
    const fileRef = this.storage.ref(`${filePath}`);

    const task = this.storage.upload(filePath, resizedImage);

    task.percentageChanges().subscribe((percentage) => {
      this.uploadPercent = percentage;
    });

    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.picture = url;
            this.toastr.success('This Uploaded');
          });
        })
      )
      .subscribe();
  }
}
