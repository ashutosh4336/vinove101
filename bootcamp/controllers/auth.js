const User = require('../models/User');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

// Get token from model, create cookie and send response

const sendTokenResponse = (user, statusCode, res) => {
  // Create Token
  const token = user.getSignedJwtToken();

  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };

  if (process.env.NODE_ENV === 'production') {
    options.secure = true;
  }

  res
    .status(statusCode)
    .cookie('token', token, options)
    .json({ success: true, token });
};

// @desc        Register a User
// @route       POST api/v1/auth/register
// @access      Public

exports.register = asyncHandler(async (req, res, next) => {
  const { name, email, password, role } = req.body;
  // create user
  const user = await User.create({
    name,
    email,
    password,
    role,
  });

  // Create Token
  //   const token = user.getSignedJwtToken();
  //   res.status(201).json({ success: true, token });
  sendTokenResponse(user, 200, res);
});

// @desc        User Login
// @route       POST api/v1/auth/login
// @access      Public

exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;
  //   console.log(email, password);

  // validate email and Password
  if (!email || !password)
    return next(
      new ErrorResponse(`Please Provide a Valid email and Password`, 400)
    );

  // check for user
  const user = await User.findOne({ email }).select('+password');

  if (!user) return next(new ErrorResponse(`Invalid Credential`, 401));

  //   check if password matches
  const isMatch = await user.matchPassword(password);

  if (!isMatch) return next(new ErrorResponse(`Invalid Credential`, 401));

  // Create Token
  //   const token = user.getSignedJwtToken();
  //   res.status(201).json({ success: true, token });
  sendTokenResponse(user, 200, res);
});

// @desc        Get Current Logged in User
// @route       POST api/v1/auth/me
// @access      Private

exports.getMe = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id);

  res.status(200).json({
    success: true,
    data: user,
  });
});
