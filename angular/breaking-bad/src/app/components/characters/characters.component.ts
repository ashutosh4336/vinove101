import { Component, OnInit } from '@angular/core';
import { GetcharactersService } from 'src/app/services/getcharacters.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css'],
})
export class CharactersComponent implements OnInit {
  cast: any;

  constructor(private bbad: GetcharactersService) {}

  ngOnInit(): void {
    this.bbad.getData().subscribe((el) => {
      this.cast = el;
      // console.log(this.cast);
    });
  }

  // getChars() {
  //   this.bbad.getData().subscribe((el) => {
  //     this.cast = el;
  //     console.log(this.cast);
  //   });
  // }
}
