// Void Type

function add(n1: number, n2: number) {
  return n1 + n2;
}

function printResult(num: number) {
  console.log(`The Result is : ${num}`);
}

printResult(add(9, 45));

// let combineValues: Function;
let combineValues: (p: number, q: number) => number;
combineValues = add;
// combineValues = printResult;

console.log('Combined Value is', combineValues(5, 8));

function addHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

addHandle(10, 20, (result) => console.log(result));
