const express = require("express");

const mongoDb = require("./config/db");
const Post = require("./models/Posts");
const app = express();

// connect DB
mongoDb();

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

app.post("/api/posts", async (req, res) => {
  const post = {
    title: req.body.title,
    content: req.body.content,
  };
  const createPost = await Post.create(post);
  res.status(201).json({
    msg: "Success",
    data: createPost,
  });
});

app.get("/api/posts", async (req, res, next) => {
  const posts = await Post.find();
  res.status(200).json({
    msg: "Successful",
    length: posts.length,
    data: posts,
  });
});

module.exports = app;
