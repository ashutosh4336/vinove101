export {};

class Department {
  // private id: number;
  // private name: string;
  // private employees: string[] = [];
  protected employees: string[] = [];

  // Short Hand Initilizition
  constructor(private readonly id: number, private name: string) {
    // this.id = id;
    // this.name = name;
  }

  describe(this: Department) {
    console.log(`Department name is : '${this.name}' whose id is ${this.id}`);
  }

  addEmployee(emp: string) {
    this.employees.push(emp);
  }

  printEmployeeInfo() {
    // this.id = 5; // rerurn a error because it id is readonly
    // console.log(this.employees.length);
    // console.log(this.employees);
    this.employees.forEach((a) => console.log(`${a}`));
  }
}

class ItDepartment extends Department {
  constructor(id: number, public admins: string[]) {
    super(id, 'It Department');
    this.admins;
  }

  printAdminInfo() {
    // this.admins.forEach((a) => console.log(a));
    console.log(`Admins of IT Department ${this.admins.join()}`);
  }
}

class Finance extends Department {
  private lastReport: string;

  constructor(id: number, private reports: string[]) {
    super(id, 'Finance Department');
    this.lastReport = reports[0];
  }

  addReport(text: string) {
    this.lastReport = text;
    this.reports.push(text);
  }

  getReports() {
    this.reports.forEach((a) => console.log(a));
  }

  addEmployee(name: string) {
    if (name === 'Ashutosh Panda') {
      return;
    }
    this.employees.push(name);
  }
}

const depOne = new Department(0o1, 'Development');
// depOne.describe();
depOne.addEmployee('Ashutosh Panda');
depOne.addEmployee('Shruti');
// depOne.employees[2] = 'Rahul Yadav'; // wrong if variable is priavte
// depOne.printEmployeeInfo();
// const devCopy = { name: 'Admin', describe: depOne.describe };
// devCopy.describe();

// IT Department
const itDep = new ItDepartment(0o2, ['Swapna Jha', 'Ajay Tandon']);
// itDep.describe();
// itDep.addEmployee('Bijawat');
// itDep.printAdminInfo();
// itDep.printEmployeeInfo();

// Finance Department
const finDep = new Finance(0o3, []);
finDep.describe();
finDep.addReport('Q4 was awesome');
finDep.getReports();
finDep.addEmployee('Shraddha Kapoor');

finDep.printEmployeeInfo();
