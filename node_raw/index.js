
const http = require('http');
const todo = [
  { id: 1, text: 'ToDo One' },
  { id: 2, text: 'ToDo Two' },
  { id: 3, text: 'ToDo Three' }
];
let status = 404;
const server = http.createServer((req, res) => {
  const { method, url } = req;
  let body = [];
  req
    .on('data', chunk => {
      body.push(chunk);
    })
    .on('end', () => {
      body = Buffer.concat(body).toString();
      // console.log(body);

      const response = {
        success: false,
        data: null,
        error: null
      };

      if (method === 'GET' && url === '/todos') {
        status = 200;
        response.success = true;
        response.data = todo;
      } else if (method === 'POST' && url === '/todos') {
        const { id, text } = JSON.parse(body);
        if (!id || !text) {
          status = 400;
          console.log(status);
        } else {
          todo.push({ id, text });
          status = 201;
          response.success = true;
          response.data = todo;
        }
      }
      //   res.setHeader('Content-Type', 'application/json');
      //   res.setHeader('X-Powered-By', 'Nodejs');
      //   res.write('<h1>Hello Vinove</h1>');
      //   res.write('<h2>Hello Vinove</h2>');
      //   res.statusCode = 404;
      res.writeHead(200, {
        'Content-Type': 'application/json',
        'X-Powered-By': 'Nodejs'
      });
      res.end(JSON.stringify(response));
    });
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => console.log(`Server Running on Port ${PORT}`));

// HTTP Status Code
// 1.xx Informational ==> 100:Continue, 101:Switching Protocol, 102:Processing, 103:Early Hints
// 2.xx SUccess ==> 201:Created, 204:NoContent, 202:Accepted
// 3.xx Redirection ==> 301:Moved Permanently
// 304  Not Modified
// 4.xx Client Error
// 400  Bad Request
// 403  Payment Required
// 401  Unauthoorized
// 404  NotFound
// 5.xx Server Error
// 500  Internal Server Error

