"use strict";
const u1 = { name: 'Max', age: 30 };
const u2 = { name: 'Ashutosh', age: 21 };
const p1 = { title: 'A Book', price: 12.99 };
function combine(a, b, resultConversion) {
    let res;
    if ((typeof a === 'number' && typeof b === 'number') ||
        resultConversion === 'as-number')
        res = +a + +b;
    else
        res = a.toString() + ' ' + b.toString();
    return res;
}
console.log(combine(56, 78, 'as-number'));
console.log(combine('10', '48', 'as-number'));
console.log(combine('Ashutosh', 'Panda', 'as-text'));
