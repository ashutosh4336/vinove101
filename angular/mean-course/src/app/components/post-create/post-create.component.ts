import { Component, OnInit } from "@angular/core";
import { PostService } from "src/app/services/post.service";
import { NgForm } from "@angular/forms";
// import { v4 as uuidV4 } from "uuid";

@Component({
  selector: "app-post-create",
  templateUrl: "./post-create.component.html",
  styleUrls: ["./post-create.component.css"],
})
export class PostCreateComponent implements OnInit {
  enteredTitle = "";
  enteredContent = "";

  constructor(private postServ: PostService) {}

  ngOnInit(): void {}

  async handleClick(f: NgForm) {
    const { title, content } = f.value;
    if (f.invalid) return;
    const post = {
      id: null,
      title: title,
      content: content,
    };

    // console.log(post);

    this.postServ.addPost(post);

    f.resetForm();
  }
}
