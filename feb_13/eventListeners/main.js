// function random(a, b) {
//   return Math.floor(Math.random() * 10 + a);
// }

// console.log(random(5, 12));
//  ################################################ //
// console.log('get started');
// Examine the Document Object
// console.dir(document);

// console.log(document.domain);
// console.log(document.URL);
// console.log(document.title);
// document.title = 123;
// console.log(document.doctype);

// console.log(document.head);
// console.log(document.body);

// console.log(document.all);
// console.log(document.all[10]);
// document.all[10].textContent = 'Hello';
// console.log(document.forms);
// console.log(document.links);
// console.log(document.images);

// console.log(document.getElementById('header-title'));
// const headerTitle = document.getElementById('header-title');
// const mainHeader = document.getElementById('main-header');
// console.log(headerTitle);

// headerTitle.textContent = 'Ashutosh4336';
// headerTitle.innerText = 'GoodBye';

// innerText pays attention to style
// innerText Doesn't show the content if it's hidden in CSS

// textContent don't pay attention to Style
// It shows all the text even if we hidde it in CSS

// headerTitle.innerHTML = '<h3>Hello</h3>';
// innerHTML puts the HTML code inside the Markup

// headerTitle.style.borderBottom = 'solid 3px #000';
// headerTitle.style.borderTop = 'solid 3px #000';
// headerTitle.style.width = '30%';

// mainHeader.style.borderBottom = '#000 2px solid';

// GET ELEMETS BY CLASS-NAME
// const items = document.getElementsByClassName('list-group-item');
// console.log(items);
// console.log(items[2]);
// items[1].textContent = 'Babu Bhiya';
// items[1].style.fontWeight = 'bold';
// items[1].style.backgroundColor = '#666';
// items[1].style.color = '#fff';

// // Error
// // items.style.backgroundColor = '#666';

// for (let i = 0; i < items.length; i++) {
//   items[i].style.backgroundColor = '#666';
//   items[i].style.color = '#fff';
// }

// GET ELEMENT BY TAG NAME

// const li = document.getElementsByTagName('li');

// // console.log(li[0], li[1]);
// for (let i = 0; i < li.length; i++) {
//   li[i].style.backgroundColor = '#666';
//   li[i].style.color = '#eee';
// }

// QuerySelector
// const header = document.querySelector('li');
// console.log(header);
// header.style.borderBottom = 'solid 2px #000';

// const input = document.querySelector('input');
// // input.value = 'Hello World';
// input.placeholder = 'Hello World';

// const submit = document.querySelector('input[type = "submit"]');

// submit.style.backgroundColor = 'green';
// submit.style.color = '#fff';

// const item = document.querySelector('.list-group-item');
// item.style.color = 'red';

// const lastItem = document.querySelector('.list-group-item:last-child');
// console.log(lastItem);
// lastItem.style.color = 'grey';

// const secondItem = document.querySelector('.list-group-item:nth-child(2)');
// console.log(secondItem);
// secondItem.style.color = 'green';

// const thirdItem = document.querySelector('.list-group-item:nth-child(3)');
// console.log(thirdItem);
// thirdItem.style.color = 'blue';

// querySelectorAll
// const titles = document.querySelectorAll('.title');
// console.log(titles);
// titles[1].textContent = 'Hello';
// titles[0].textContent = 'Search item';
// const allLi = document.querySelectorAll('li');
// for (let i = 0; i < allLi.length; i++) {
//   allLi[i].style.color = '#fff';
// }

// const oddLi = document.querySelectorAll('li:nth-child(odd');

// for (let i = 0; i < oddLi.length; i++) {
//   oddLi[i].style.backgroundColor = '#3e3e3e';
// }

// const evenLi = document.querySelectorAll('li:nth-child(even)');

// for (let i = 0; i < evenLi.length; i++) {
//   evenLi[i].style.backgroundColor = '#666';
// }

// Traversing The Dom  //

const itemList = document.querySelector('#items');
//ParentNode Property
// console.log(itemList.parentNode);
// itemList.parentNode.style.backgroundColor = '#666';
// itemList.parentNode.style.color = 'grey';
// console.log(itemList.parentNode.parentNode.parentNode.nodeName);
// Container
// console.log(itemList.parentNode.parentNode.parentNode); BODY
// console.log(itemList.parentElement.parentElement.parentElement.nodeName);
// parentNode and paarentElement rae Mostly Same

// CHILDNODE childNode

// console.log(itemList.childNodes);

// console.log(itemList.children);
// console.log(itemList.children[1]);
// itemList.children[1].style.backgroundColor = 'green';
// itemList.children[1].style.color = '#fff';

// Gives Back a text node
// console.log(itemList.firstChild);

// console.log(itemList.firstElementChild);
// itemList.firstElementChild.textContent = 'Hello One';

// console.log(itemList.lastChild);
// console.log(itemList.lastElementChild);
// itemList.lastElementChild.textContent = 'LastOne';

// nextSibling Returns a TextNode
// console.log(itemList.nextSibling);

// console.log(itemList.nextElementSibling);
// console.log(itemList.previousSibling); //TextNode
// console.log(itemList.previousElementSibling);

// ********************************************* //
// CreateElement
// // create a Div
// const newDiv = document.createElement('div');
// newDiv.className = 'bg-dark text-center lead p-3 rounded'; //add class
// newDiv.id = 'hello';

// // Add attribute
// newDiv.setAttribute('title', 'Hello Div');
// newDiv.setAttribute('for', 'name');

// // Create a TextNode
// const newTextDiv = document.createTextNode('Panda Market Complex');

// //add text to div
// newDiv.appendChild(newTextDiv);

// const container = document.querySelector('header .container');
// const h1 = document.querySelector('header h1');

// container.insertBefore(newDiv, h1);

// // console.log(newTextDiv);
// // console.log(newDiv);

// // ******************************************************* //
// // Create a Element
// const secondDiv = document.createElement('div');
// secondDiv.className = 'bg-info p-5 display-1 text-white text-center rounded';
// secondDiv.id = 'ashutosh';
// // setAttribute
// secondDiv.setAttribute('for', 'email');

// // Add a textnode
// const secondText = document.createTextNode('Hello Vinove');

// // Append Child
// secondDiv.appendChild(secondText);

// const card = document.querySelector('#main');
// const h2 = document.querySelector('#title');

// card.insertBefore(secondDiv, h2);

// console.log(secondDiv);

// EventListener

// function btnClick() {
//   console.log('Button Clicked');
// }

// const btn = document.getElementById('button');

// btn.addEventListener('click', e => {
//   const title = document.getElementById('header-title');
//   title.parentNode.parentNode.className = 'bg-primary text-white p-4 mb-3';
//   title.textContent = 'Panda Market Complex';
//   console.log(e);
// console.log(e.target);
// console.log(e.target.id);
// console.log(e.altKey);
// console.log(e.shiftKey);
// });

// btn.addEventListener('dblclick', e => {
//   const title = document.getElementById('header-title');
//   title.parentNode.parentNode.className = 'bg-info text-white p-4 mb-3';
//   title.textContent = 'Hello Amigos';
// });

const btn = document.getElementById('button');
const box = document.getElementById('box');
// const output = document.getElementById('output');

const itemInput = document.querySelector('input[type="text"]');
const form = document.querySelector('form');
const select = document.querySelector('select');
// btn.addEventListener('click', runEvent);
// btn.addEventListener('dblclick', runEvent);
// btn.addEventListener('mousedown', runEvent);
// btn.addEventListener('mouseup', runEvent);
// itemInput.addEventListener('keydown', runEvent);
// itemInput.addEventListener('keyup', runEvent);
// itemInput.addEventListener('keypress', runEvent);
// itemInput.addEventListener('focus', runEvent);
// itemInput.addEventListener('blur', runEvent);
// itemInput.addEventListener('cut', runEvent);
// itemInput.addEventListener('copy', runEvent);
// itemInput.addEventListener('paste', runEvent);
// itemInput.addEventListener('input', runEvent);
// select.addEventListener('change', runEvent);
form.addEventListener('submit', runEvent);

function runEvent(e) {
  e.preventDefault();
  // console.log('Event Type: ' + e.type);
  // console.log(e.target.value);
  // document.getElementById('output').innerHTML = `<h3>${e.target.value}</h3>`;
  // output.innerHTML =
  //   '<h3>MouseX: ' + e.offsetX + '</h3><h3>MouseY: ' + e.offsetY + '</h3>';
  // console.log(output);
  // document.body.style.backgroundColor =
  // 'rgb(' + e.offsetX + ', ' + e.offsetY + ',50)';
}

// box.addEventListener('mouseenter', runEvent);
// box.addEventListener('mouseleave', runEvent);

// box.addEventListener('mouseover', runEvent);
// box.addEventListener('mouseout', runEvent);

//
